/**
 * <copyright>
 * </copyright>
 *
 * $Id: SitecheckerschemaFactory.java,v 1.1 2011/01/17 20:25:51 aeast Exp $
 */
package sitecheck.usatoday.com.sitecheckerschema;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage
 * @generated
 */
public interface SitecheckerschemaFactory extends EFactory
{
  /**
   * The singleton instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  SitecheckerschemaFactory eINSTANCE = sitecheck.usatoday.com.sitecheckerschema.impl.SitecheckerschemaFactoryImpl.init();

  /**
   * Returns a new object of class '<em>Alert List Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Alert List Type</em>'.
   * @generated
   */
  AlertListType createAlertListType();

  /**
   * Returns a new object of class '<em>Alert Receiver Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Alert Receiver Type</em>'.
   * @generated
   */
  AlertReceiverType createAlertReceiverType();

  /**
   * Returns a new object of class '<em>Document Root</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Document Root</em>'.
   * @generated
   */
  DocumentRoot createDocumentRoot();

  /**
   * Returns a new object of class '<em>Error Page List</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Error Page List</em>'.
   * @generated
   */
  ErrorPageList createErrorPageList();

  /**
   * Returns a new object of class '<em>Site Checker Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Site Checker Type</em>'.
   * @generated
   */
  SiteCheckerType createSiteCheckerType();

  /**
   * Returns a new object of class '<em>Site Checker Type1</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Site Checker Type1</em>'.
   * @generated
   */
  SiteCheckerType1 createSiteCheckerType1();

  /**
   * Returns a new object of class '<em>Site Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Site Type</em>'.
   * @generated
   */
  SiteType createSiteType();

  /**
   * Returns a new object of class '<em>Smtp Provider Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>Smtp Provider Type</em>'.
   * @generated
   */
  SmtpProviderType createSmtpProviderType();

  /**
   * Returns a new object of class '<em>URL Sequence</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>URL Sequence</em>'.
   * @generated
   */
  URLSequence createURLSequence();

  /**
   * Returns a new object of class '<em>URL Type</em>'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return a new object of class '<em>URL Type</em>'.
   * @generated
   */
  URLType createURLType();

  /**
   * Returns the package supported by this factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the package supported by this factory.
   * @generated
   */
  SitecheckerschemaPackage getSitecheckerschemaPackage();

} //SitecheckerschemaFactory
