/**
 * <copyright>
 * </copyright>
 *
 * $Id: AlertListType.java,v 1.1 2011/01/17 20:25:51 aeast Exp $
 */
package sitecheck.usatoday.com.sitecheckerschema;

import java.util.List;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alert List Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.AlertListType#getAlertReceiverEmail <em>Alert Receiver Email</em>}</li>
 * </ul>
 * </p>
 *
 * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getAlertListType()
 * @model extendedMetaData="name='AlertListType' kind='elementOnly'"
 * @generated
 */
public interface AlertListType
{
  /**
   * Returns the value of the '<em><b>Alert Receiver Email</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Alert Receiver Email</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Alert Receiver Email</em>' attribute list.
   * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getAlertListType_AlertReceiverEmail()
   * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
   *        extendedMetaData="kind='element' name='AlertReceiverEmail'"
   * @generated
   */
  List<String> getAlertReceiverEmail();

} // AlertListType
