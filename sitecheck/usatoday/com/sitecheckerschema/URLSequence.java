/**
 * <copyright>
 * </copyright>
 *
 * $Id: URLSequence.java,v 1.1 2011/01/17 20:25:51 aeast Exp $
 */
package sitecheck.usatoday.com.sitecheckerschema;

import java.util.List;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>URL Sequence</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.URLSequence#getURL <em>URL</em>}</li>
 * </ul>
 * </p>
 *
 * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getURLSequence()
 * @model extendedMetaData="name='URLSequence' kind='elementOnly'"
 * @generated
 */
public interface URLSequence
{
  /**
   * Returns the value of the '<em><b>URL</b></em>' containment reference list.
   * The list contents are of type {@link sitecheck.usatoday.com.sitecheckerschema.URLType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>URL</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>URL</em>' containment reference list.
   * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getURLSequence_URL()
   * @model containment="true" required="true"
   *        extendedMetaData="kind='element' name='URL'"
   * @generated
   */
  List<URLType> getURL();

} // URLSequence
