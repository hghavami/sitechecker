/**
 * <copyright>
 * </copyright>
 *
 * $Id: AlertReceiverType.java,v 1.1 2011/01/17 20:25:51 aeast Exp $
 */
package sitecheck.usatoday.com.sitecheckerschema;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Alert Receiver Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.AlertReceiverType#getEmailAddress <em>Email Address</em>}</li>
 * </ul>
 * </p>
 *
 * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getAlertReceiverType()
 * @model extendedMetaData="name='AlertReceiverType' kind='elementOnly'"
 * @generated
 */
public interface AlertReceiverType
{
  /**
   * Returns the value of the '<em><b>Email Address</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Email Address</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Email Address</em>' attribute.
   * @see #setEmailAddress(String)
   * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getAlertReceiverType_EmailAddress()
   * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
   *        extendedMetaData="kind='element' name='EmailAddress'"
   * @generated
   */
  String getEmailAddress();

  /**
   * Sets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.AlertReceiverType#getEmailAddress <em>Email Address</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Email Address</em>' attribute.
   * @see #getEmailAddress()
   * @generated
   */
  void setEmailAddress(String value);

} // AlertReceiverType
