/**
 * <copyright>
 * </copyright>
 *
 * $Id: SiteType.java,v 1.1 2011/01/17 20:25:51 aeast Exp $
 */
package sitecheck.usatoday.com.sitecheckerschema;

import java.math.BigInteger;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Site Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getURLPath <em>URL Path</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getCheckInterval <em>Check Interval</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getResponseTimeThreshold <em>Response Time Threshold</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getUnableToConnectRetryAttempts <em>Unable To Connect Retry Attempts</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getAlertList <em>Alert List</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getAlertInterval <em>Alert Interval</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getLogFile <em>Log File</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getLogLevel <em>Log Level</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#isNotifyWhenUp <em>Notify When Up</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getErrorPages <em>Error Pages</em>}</li>
 * </ul>
 * </p>
 *
 * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getSiteType()
 * @model extendedMetaData="name='SiteType' kind='elementOnly'"
 * @generated
 */
public interface SiteType
{
  /**
   * Returns the value of the '<em><b>URL Path</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>URL Path</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>URL Path</em>' containment reference.
   * @see #setURLPath(URLSequence)
   * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getSiteType_URLPath()
   * @model containment="true" required="true"
   *        extendedMetaData="kind='element' name='URLPath'"
   * @generated
   */
  URLSequence getURLPath();

  /**
   * Sets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getURLPath <em>URL Path</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>URL Path</em>' containment reference.
   * @see #getURLPath()
   * @generated
   */
  void setURLPath(URLSequence value);

  /**
   * Returns the value of the '<em><b>Check Interval</b></em>' attribute.
   * The default value is <code>"300"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Check Interval</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Check Interval</em>' attribute.
   * @see #isSetCheckInterval()
   * @see #unsetCheckInterval()
   * @see #setCheckInterval(BigInteger)
   * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getSiteType_CheckInterval()
   * @model default="300" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.PositiveInteger" required="true"
   *        extendedMetaData="kind='element' name='CheckInterval'"
   * @generated
   */
  BigInteger getCheckInterval();

  /**
   * Sets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getCheckInterval <em>Check Interval</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Check Interval</em>' attribute.
   * @see #isSetCheckInterval()
   * @see #unsetCheckInterval()
   * @see #getCheckInterval()
   * @generated
   */
  void setCheckInterval(BigInteger value);

  /**
   * Unsets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getCheckInterval <em>Check Interval</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetCheckInterval()
   * @see #getCheckInterval()
   * @see #setCheckInterval(BigInteger)
   * @generated
   */
  void unsetCheckInterval();

  /**
   * Returns whether the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getCheckInterval <em>Check Interval</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Check Interval</em>' attribute is set.
   * @see #unsetCheckInterval()
   * @see #getCheckInterval()
   * @see #setCheckInterval(BigInteger)
   * @generated
   */
  boolean isSetCheckInterval();

  /**
   * Returns the value of the '<em><b>Response Time Threshold</b></em>' attribute.
   * The default value is <code>"60"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Response Time Threshold</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Response Time Threshold</em>' attribute.
   * @see #isSetResponseTimeThreshold()
   * @see #unsetResponseTimeThreshold()
   * @see #setResponseTimeThreshold(BigInteger)
   * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getSiteType_ResponseTimeThreshold()
   * @model default="60" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.PositiveInteger" required="true"
   *        extendedMetaData="kind='element' name='ResponseTimeThreshold'"
   * @generated
   */
  BigInteger getResponseTimeThreshold();

  /**
   * Sets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getResponseTimeThreshold <em>Response Time Threshold</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Response Time Threshold</em>' attribute.
   * @see #isSetResponseTimeThreshold()
   * @see #unsetResponseTimeThreshold()
   * @see #getResponseTimeThreshold()
   * @generated
   */
  void setResponseTimeThreshold(BigInteger value);

  /**
   * Unsets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getResponseTimeThreshold <em>Response Time Threshold</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetResponseTimeThreshold()
   * @see #getResponseTimeThreshold()
   * @see #setResponseTimeThreshold(BigInteger)
   * @generated
   */
  void unsetResponseTimeThreshold();

  /**
   * Returns whether the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getResponseTimeThreshold <em>Response Time Threshold</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Response Time Threshold</em>' attribute is set.
   * @see #unsetResponseTimeThreshold()
   * @see #getResponseTimeThreshold()
   * @see #setResponseTimeThreshold(BigInteger)
   * @generated
   */
  boolean isSetResponseTimeThreshold();

  /**
   * Returns the value of the '<em><b>Unable To Connect Retry Attempts</b></em>' attribute.
   * The default value is <code>"30"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Unable To Connect Retry Attempts</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Unable To Connect Retry Attempts</em>' attribute.
   * @see #isSetUnableToConnectRetryAttempts()
   * @see #unsetUnableToConnectRetryAttempts()
   * @see #setUnableToConnectRetryAttempts(int)
   * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getSiteType_UnableToConnectRetryAttempts()
   * @model default="30" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Int" required="true"
   *        extendedMetaData="kind='element' name='UnableToConnectRetryAttempts'"
   * @generated
   */
  int getUnableToConnectRetryAttempts();

  /**
   * Sets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getUnableToConnectRetryAttempts <em>Unable To Connect Retry Attempts</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Unable To Connect Retry Attempts</em>' attribute.
   * @see #isSetUnableToConnectRetryAttempts()
   * @see #unsetUnableToConnectRetryAttempts()
   * @see #getUnableToConnectRetryAttempts()
   * @generated
   */
  void setUnableToConnectRetryAttempts(int value);

  /**
   * Unsets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getUnableToConnectRetryAttempts <em>Unable To Connect Retry Attempts</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetUnableToConnectRetryAttempts()
   * @see #getUnableToConnectRetryAttempts()
   * @see #setUnableToConnectRetryAttempts(int)
   * @generated
   */
  void unsetUnableToConnectRetryAttempts();

  /**
   * Returns whether the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getUnableToConnectRetryAttempts <em>Unable To Connect Retry Attempts</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Unable To Connect Retry Attempts</em>' attribute is set.
   * @see #unsetUnableToConnectRetryAttempts()
   * @see #getUnableToConnectRetryAttempts()
   * @see #setUnableToConnectRetryAttempts(int)
   * @generated
   */
  boolean isSetUnableToConnectRetryAttempts();

  /**
   * Returns the value of the '<em><b>Alert List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Alert List</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Alert List</em>' containment reference.
   * @see #setAlertList(AlertListType)
   * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getSiteType_AlertList()
   * @model containment="true" required="true"
   *        extendedMetaData="kind='element' name='AlertList'"
   * @generated
   */
  AlertListType getAlertList();

  /**
   * Sets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getAlertList <em>Alert List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Alert List</em>' containment reference.
   * @see #getAlertList()
   * @generated
   */
  void setAlertList(AlertListType value);

  /**
   * Returns the value of the '<em><b>Alert Interval</b></em>' attribute.
   * The default value is <code>"10"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Alert Interval</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Alert Interval</em>' attribute.
   * @see #isSetAlertInterval()
   * @see #unsetAlertInterval()
   * @see #setAlertInterval(BigInteger)
   * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getSiteType_AlertInterval()
   * @model default="10" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.PositiveInteger" required="true"
   *        extendedMetaData="kind='element' name='AlertInterval'"
   * @generated
   */
  BigInteger getAlertInterval();

  /**
   * Sets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getAlertInterval <em>Alert Interval</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Alert Interval</em>' attribute.
   * @see #isSetAlertInterval()
   * @see #unsetAlertInterval()
   * @see #getAlertInterval()
   * @generated
   */
  void setAlertInterval(BigInteger value);

  /**
   * Unsets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getAlertInterval <em>Alert Interval</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetAlertInterval()
   * @see #getAlertInterval()
   * @see #setAlertInterval(BigInteger)
   * @generated
   */
  void unsetAlertInterval();

  /**
   * Returns whether the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getAlertInterval <em>Alert Interval</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Alert Interval</em>' attribute is set.
   * @see #unsetAlertInterval()
   * @see #getAlertInterval()
   * @see #setAlertInterval(BigInteger)
   * @generated
   */
  boolean isSetAlertInterval();

  /**
   * Returns the value of the '<em><b>Log File</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Log File</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Log File</em>' attribute.
   * @see #isSetLogFile()
   * @see #unsetLogFile()
   * @see #setLogFile(String)
   * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getSiteType_LogFile()
   * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
   *        extendedMetaData="kind='element' name='LogFile'"
   * @generated
   */
  String getLogFile();

  /**
   * Sets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getLogFile <em>Log File</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Log File</em>' attribute.
   * @see #isSetLogFile()
   * @see #unsetLogFile()
   * @see #getLogFile()
   * @generated
   */
  void setLogFile(String value);

  /**
   * Unsets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getLogFile <em>Log File</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetLogFile()
   * @see #getLogFile()
   * @see #setLogFile(String)
   * @generated
   */
  void unsetLogFile();

  /**
   * Returns whether the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getLogFile <em>Log File</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Log File</em>' attribute is set.
   * @see #unsetLogFile()
   * @see #getLogFile()
   * @see #setLogFile(String)
   * @generated
   */
  boolean isSetLogFile();

  /**
   * Returns the value of the '<em><b>Log Level</b></em>' attribute.
   * The default value is <code>"INFO"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Log Level</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Log Level</em>' attribute.
   * @see #isSetLogLevel()
   * @see #unsetLogLevel()
   * @see #setLogLevel(String)
   * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getSiteType_LogLevel()
   * @model default="INFO" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
   *        extendedMetaData="kind='element' name='LogLevel'"
   * @generated
   */
  String getLogLevel();

  /**
   * Sets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getLogLevel <em>Log Level</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Log Level</em>' attribute.
   * @see #isSetLogLevel()
   * @see #unsetLogLevel()
   * @see #getLogLevel()
   * @generated
   */
  void setLogLevel(String value);

  /**
   * Unsets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getLogLevel <em>Log Level</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetLogLevel()
   * @see #getLogLevel()
   * @see #setLogLevel(String)
   * @generated
   */
  void unsetLogLevel();

  /**
   * Returns whether the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getLogLevel <em>Log Level</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Log Level</em>' attribute is set.
   * @see #unsetLogLevel()
   * @see #getLogLevel()
   * @see #setLogLevel(String)
   * @generated
   */
  boolean isSetLogLevel();

  /**
   * Returns the value of the '<em><b>Notify When Up</b></em>' attribute.
   * The default value is <code>"true"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Notify When Up</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Notify When Up</em>' attribute.
   * @see #isSetNotifyWhenUp()
   * @see #unsetNotifyWhenUp()
   * @see #setNotifyWhenUp(boolean)
   * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getSiteType_NotifyWhenUp()
   * @model default="true" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.Boolean" required="true"
   *        extendedMetaData="kind='element' name='NotifyWhenUp'"
   * @generated
   */
  boolean isNotifyWhenUp();

  /**
   * Sets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#isNotifyWhenUp <em>Notify When Up</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Notify When Up</em>' attribute.
   * @see #isSetNotifyWhenUp()
   * @see #unsetNotifyWhenUp()
   * @see #isNotifyWhenUp()
   * @generated
   */
  void setNotifyWhenUp(boolean value);

  /**
   * Unsets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#isNotifyWhenUp <em>Notify When Up</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetNotifyWhenUp()
   * @see #isNotifyWhenUp()
   * @see #setNotifyWhenUp(boolean)
   * @generated
   */
  void unsetNotifyWhenUp();

  /**
   * Returns whether the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#isNotifyWhenUp <em>Notify When Up</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Notify When Up</em>' attribute is set.
   * @see #unsetNotifyWhenUp()
   * @see #isNotifyWhenUp()
   * @see #setNotifyWhenUp(boolean)
   * @generated
   */
  boolean isSetNotifyWhenUp();

  /**
   * Returns the value of the '<em><b>Error Pages</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Error Pages</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Error Pages</em>' containment reference.
   * @see #setErrorPages(ErrorPageList)
   * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getSiteType_ErrorPages()
   * @model containment="true" required="true"
   *        extendedMetaData="kind='element' name='ErrorPages'"
   * @generated
   */
  ErrorPageList getErrorPages();

  /**
   * Sets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getErrorPages <em>Error Pages</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Error Pages</em>' containment reference.
   * @see #getErrorPages()
   * @generated
   */
  void setErrorPages(ErrorPageList value);

} // SiteType
