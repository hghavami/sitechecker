/**
 * <copyright>
 * </copyright>
 *
 * $Id: SitecheckerschemaFactoryImpl.java,v 1.1 2011/01/17 20:25:51 aeast Exp $
 */
package sitecheck.usatoday.com.sitecheckerschema.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;
import sitecheck.usatoday.com.sitecheckerschema.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SitecheckerschemaFactoryImpl extends EFactoryImpl implements SitecheckerschemaFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static SitecheckerschemaFactory init()
  {
    try
    {
      SitecheckerschemaFactory theSitecheckerschemaFactory = (SitecheckerschemaFactory)EPackage.Registry.INSTANCE.getEFactory("http://com.usatoday.sitecheck/sitecheckerschema"); 
      if (theSitecheckerschemaFactory != null)
      {
        return theSitecheckerschemaFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new SitecheckerschemaFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SitecheckerschemaFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case SitecheckerschemaPackage.ALERT_LIST_TYPE: return (EObject)createAlertListType();
      case SitecheckerschemaPackage.ALERT_RECEIVER_TYPE: return (EObject)createAlertReceiverType();
      case SitecheckerschemaPackage.DOCUMENT_ROOT: return (EObject)createDocumentRoot();
      case SitecheckerschemaPackage.ERROR_PAGE_LIST: return (EObject)createErrorPageList();
      case SitecheckerschemaPackage.SITE_CHECKER_TYPE: return (EObject)createSiteCheckerType();
      case SitecheckerschemaPackage.SITE_CHECKER_TYPE1: return (EObject)createSiteCheckerType1();
      case SitecheckerschemaPackage.SITE_TYPE: return (EObject)createSiteType();
      case SitecheckerschemaPackage.SMTP_PROVIDER_TYPE: return (EObject)createSmtpProviderType();
      case SitecheckerschemaPackage.URL_SEQUENCE: return (EObject)createURLSequence();
      case SitecheckerschemaPackage.URL_TYPE: return (EObject)createURLType();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AlertListType createAlertListType()
  {
    AlertListTypeImpl alertListType = new AlertListTypeImpl();
    return alertListType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AlertReceiverType createAlertReceiverType()
  {
    AlertReceiverTypeImpl alertReceiverType = new AlertReceiverTypeImpl();
    return alertReceiverType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public DocumentRoot createDocumentRoot()
  {
    DocumentRootImpl documentRoot = new DocumentRootImpl();
    return documentRoot;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ErrorPageList createErrorPageList()
  {
    ErrorPageListImpl errorPageList = new ErrorPageListImpl();
    return errorPageList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SiteCheckerType createSiteCheckerType()
  {
    SiteCheckerTypeImpl siteCheckerType = new SiteCheckerTypeImpl();
    return siteCheckerType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SiteCheckerType1 createSiteCheckerType1()
  {
    SiteCheckerType1Impl siteCheckerType1 = new SiteCheckerType1Impl();
    return siteCheckerType1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SiteType createSiteType()
  {
    SiteTypeImpl siteType = new SiteTypeImpl();
    return siteType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SmtpProviderType createSmtpProviderType()
  {
    SmtpProviderTypeImpl smtpProviderType = new SmtpProviderTypeImpl();
    return smtpProviderType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public URLSequence createURLSequence()
  {
    URLSequenceImpl urlSequence = new URLSequenceImpl();
    return urlSequence;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public URLType createURLType()
  {
    URLTypeImpl urlType = new URLTypeImpl();
    return urlType;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SitecheckerschemaPackage getSitecheckerschemaPackage()
  {
    return (SitecheckerschemaPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static SitecheckerschemaPackage getPackage()
  {
    return SitecheckerschemaPackage.eINSTANCE;
  }

} //SitecheckerschemaFactoryImpl
