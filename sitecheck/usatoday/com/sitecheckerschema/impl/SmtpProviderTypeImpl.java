/**
 * <copyright>
 * </copyright>
 *
 * $Id: SmtpProviderTypeImpl.java,v 1.1 2011/01/17 20:25:51 aeast Exp $
 */
package sitecheck.usatoday.com.sitecheckerschema.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.sdo.impl.EDataObjectImpl;

import sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage;
import sitecheck.usatoday.com.sitecheckerschema.SmtpProviderType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Smtp Provider Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.impl.SmtpProviderTypeImpl#getFromAddress <em>From Address</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.impl.SmtpProviderTypeImpl#getHost <em>Host</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SmtpProviderTypeImpl extends EDataObjectImpl implements SmtpProviderType
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static final long serialVersionUID = 1L;

  /**
   * The default value of the '{@link #getFromAddress() <em>From Address</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFromAddress()
   * @generated
   * @ordered
   */
  protected static final String FROM_ADDRESS_EDEFAULT = "aeast@usatoday.com";

  /**
   * The cached value of the '{@link #getFromAddress() <em>From Address</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getFromAddress()
   * @generated
   * @ordered
   */
  protected String fromAddress = FROM_ADDRESS_EDEFAULT;

  /**
   * This is true if the From Address attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean fromAddressESet;

  /**
   * The default value of the '{@link #getHost() <em>Host</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getHost()
   * @generated
   * @ordered
   */
  protected static final String HOST_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getHost() <em>Host</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getHost()
   * @generated
   * @ordered
   */
  protected String host = HOST_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SmtpProviderTypeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SitecheckerschemaPackage.Literals.SMTP_PROVIDER_TYPE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getFromAddress()
  {
    return fromAddress;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setFromAddress(String newFromAddress)
  {
    String oldFromAddress = fromAddress;
    fromAddress = newFromAddress;
    boolean oldFromAddressESet = fromAddressESet;
    fromAddressESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SitecheckerschemaPackage.SMTP_PROVIDER_TYPE__FROM_ADDRESS, oldFromAddress, fromAddress, !oldFromAddressESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetFromAddress()
  {
    String oldFromAddress = fromAddress;
    boolean oldFromAddressESet = fromAddressESet;
    fromAddress = FROM_ADDRESS_EDEFAULT;
    fromAddressESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SitecheckerschemaPackage.SMTP_PROVIDER_TYPE__FROM_ADDRESS, oldFromAddress, FROM_ADDRESS_EDEFAULT, oldFromAddressESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetFromAddress()
  {
    return fromAddressESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getHost()
  {
    return host;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setHost(String newHost)
  {
    String oldHost = host;
    host = newHost;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SitecheckerschemaPackage.SMTP_PROVIDER_TYPE__HOST, oldHost, host));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.SMTP_PROVIDER_TYPE__FROM_ADDRESS:
        return getFromAddress();
      case SitecheckerschemaPackage.SMTP_PROVIDER_TYPE__HOST:
        return getHost();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.SMTP_PROVIDER_TYPE__FROM_ADDRESS:
        setFromAddress((String)newValue);
        return;
      case SitecheckerschemaPackage.SMTP_PROVIDER_TYPE__HOST:
        setHost((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.SMTP_PROVIDER_TYPE__FROM_ADDRESS:
        unsetFromAddress();
        return;
      case SitecheckerschemaPackage.SMTP_PROVIDER_TYPE__HOST:
        setHost(HOST_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.SMTP_PROVIDER_TYPE__FROM_ADDRESS:
        return isSetFromAddress();
      case SitecheckerschemaPackage.SMTP_PROVIDER_TYPE__HOST:
        return HOST_EDEFAULT == null ? host != null : !HOST_EDEFAULT.equals(host);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (fromAddress: ");
    if (fromAddressESet) result.append(fromAddress); else result.append("<unset>");
    result.append(", host: ");
    result.append(host);
    result.append(')');
    return result.toString();
  }

} //SmtpProviderTypeImpl
