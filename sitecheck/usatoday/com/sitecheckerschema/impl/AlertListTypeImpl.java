/**
 * <copyright>
 * </copyright>
 *
 * $Id: AlertListTypeImpl.java,v 1.1 2011/01/17 20:25:51 aeast Exp $
 */
package sitecheck.usatoday.com.sitecheckerschema.impl;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.sdo.impl.EDataObjectImpl;
import org.eclipse.emf.ecore.util.EDataTypeEList;

import sitecheck.usatoday.com.sitecheckerschema.AlertListType;
import sitecheck.usatoday.com.sitecheckerschema.AlertReceiverType;
import sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Alert List Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.impl.AlertListTypeImpl#getAlertReceiverEmail <em>Alert Receiver Email</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AlertListTypeImpl extends EDataObjectImpl implements AlertListType
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static final long serialVersionUID = 1L;
  /**
   * The cached value of the '{@link #getAlertReceiverEmail() <em>Alert Receiver Email</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAlertReceiverEmail()
   * @generated
   * @ordered
   */
  protected EList<String> alertReceiverEmail;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AlertListTypeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SitecheckerschemaPackage.Literals.ALERT_LIST_TYPE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
public List<String> getAlertReceiverEmail()
  {
    if (alertReceiverEmail == null)
    {
      alertReceiverEmail = new EDataTypeEList<String>(String.class, this, SitecheckerschemaPackage.ALERT_LIST_TYPE__ALERT_RECEIVER_EMAIL);
    }
    return alertReceiverEmail;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.ALERT_LIST_TYPE__ALERT_RECEIVER_EMAIL:
        return getAlertReceiverEmail();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.ALERT_LIST_TYPE__ALERT_RECEIVER_EMAIL:
        getAlertReceiverEmail().clear();
        getAlertReceiverEmail().addAll((Collection<? extends String>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.ALERT_LIST_TYPE__ALERT_RECEIVER_EMAIL:
        getAlertReceiverEmail().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.ALERT_LIST_TYPE__ALERT_RECEIVER_EMAIL:
        return alertReceiverEmail != null && !alertReceiverEmail.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (alertReceiverEmail: ");
    result.append(alertReceiverEmail);
    result.append(')');
    return result.toString();
  }

} //AlertListTypeImpl
