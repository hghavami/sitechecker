/**
 * <copyright>
 * </copyright>
 *
 * $Id: ErrorPageListImpl.java,v 1.1 2011/01/17 20:25:51 aeast Exp $
 */
package sitecheck.usatoday.com.sitecheckerschema.impl;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.sdo.impl.EDataObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeEList;

import sitecheck.usatoday.com.sitecheckerschema.ErrorPageList;
import sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Error Page List</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.impl.ErrorPageListImpl#getErrorPage <em>Error Page</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ErrorPageListImpl extends EDataObjectImpl implements ErrorPageList
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static final long serialVersionUID = 1L;
  /**
   * The cached value of the '{@link #getErrorPage() <em>Error Page</em>}' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getErrorPage()
   * @generated
   * @ordered
   */
  protected EList<String> errorPage;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ErrorPageListImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SitecheckerschemaPackage.Literals.ERROR_PAGE_LIST;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
public List<String> getErrorPage()
  {
    if (errorPage == null)
    {
      errorPage = new EDataTypeEList<String>(String.class, this, SitecheckerschemaPackage.ERROR_PAGE_LIST__ERROR_PAGE);
    }
    return errorPage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.ERROR_PAGE_LIST__ERROR_PAGE:
        return getErrorPage();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.ERROR_PAGE_LIST__ERROR_PAGE:
        getErrorPage().clear();
        getErrorPage().addAll((Collection<? extends String>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.ERROR_PAGE_LIST__ERROR_PAGE:
        getErrorPage().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.ERROR_PAGE_LIST__ERROR_PAGE:
        return errorPage != null && !errorPage.isEmpty();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (errorPage: ");
    result.append(errorPage);
    result.append(')');
    return result.toString();
  }

} //ErrorPageListImpl
