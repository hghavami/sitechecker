/**
 * <copyright>
 * </copyright>
 *
 * $Id: SitecheckerschemaPackageImpl.java,v 1.1 2011/01/17 20:25:51 aeast Exp $
 */
package sitecheck.usatoday.com.sitecheckerschema.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.eclipse.emf.ecore.xml.type.XMLTypePackage;

import org.eclipse.emf.ecore.xml.type.impl.XMLTypePackageImpl;

import sitecheck.usatoday.com.sitecheckerschema.AlertListType;
import sitecheck.usatoday.com.sitecheckerschema.AlertReceiverType;
import sitecheck.usatoday.com.sitecheckerschema.DocumentRoot;
import sitecheck.usatoday.com.sitecheckerschema.ErrorPageList;
import sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType;
import sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType1;
import sitecheck.usatoday.com.sitecheckerschema.SiteType;
import sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaFactory;
import sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage;
import sitecheck.usatoday.com.sitecheckerschema.SmtpProviderType;
import sitecheck.usatoday.com.sitecheckerschema.URLSequence;
import sitecheck.usatoday.com.sitecheckerschema.URLType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SitecheckerschemaPackageImpl extends EPackageImpl implements SitecheckerschemaPackage
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass alertListTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass alertReceiverTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass documentRootEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass errorPageListEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass siteCheckerTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass siteCheckerType1EClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass siteTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass smtpProviderTypeEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass urlSequenceEClass = null;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private EClass urlTypeEClass = null;

  /**
   * Creates an instance of the model <b>Package</b>, registered with
   * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
   * package URI value.
   * <p>Note: the correct way to create the package is via the static
   * factory method {@link #init init()}, which also performs
   * initialization of the package, or returns the registered package,
   * if one already exists.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.eclipse.emf.ecore.EPackage.Registry
   * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#eNS_URI
   * @see #init()
   * @generated
   */
  private SitecheckerschemaPackageImpl()
  {
    super(eNS_URI, SitecheckerschemaFactory.eINSTANCE);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static boolean isInited = false;

  /**
   * Creates, registers, and initializes the <b>Package</b> for this
   * model, and for any others upon which it depends.  Simple
   * dependencies are satisfied by calling this method on all
   * dependent packages before doing anything else.  This method drives
   * initialization for interdependent packages directly, in parallel
   * with this package, itself.
   * <p>Of this package and its interdependencies, all packages which
   * have not yet been registered by their URI values are first created
   * and registered.  The packages are then initialized in two steps:
   * meta-model objects for all of the packages are created before any
   * are initialized, since one package's meta-model objects may refer to
   * those of another.
   * <p>Invocation of this method will not affect any packages that have
   * already been initialized.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #eNS_URI
   * @see #createPackageContents()
   * @see #initializePackageContents()
   * @generated
   */
  public static SitecheckerschemaPackage init()
  {
    if (isInited) return (SitecheckerschemaPackage)EPackage.Registry.INSTANCE.getEPackage(SitecheckerschemaPackage.eNS_URI);

    // Obtain or create and register package
    SitecheckerschemaPackageImpl theSitecheckerschemaPackage = (SitecheckerschemaPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(eNS_URI) instanceof SitecheckerschemaPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(eNS_URI) : new SitecheckerschemaPackageImpl());

    isInited = true;

    // Initialize simple dependencies
    XMLTypePackage.eINSTANCE.eClass();

    // Create package meta-data objects
    theSitecheckerschemaPackage.createPackageContents();

    // Initialize created meta-data
    theSitecheckerschemaPackage.initializePackageContents();

    // Mark meta-data to indicate it can't be changed
    theSitecheckerschemaPackage.freeze();

    return theSitecheckerschemaPackage;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAlertListType()
  {
    return alertListTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getAlertListType_AlertReceiverEmail()
  {
    return (EAttribute)alertListTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getAlertReceiverType()
  {
    return alertReceiverTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getAlertReceiverType_EmailAddress()
  {
    return (EAttribute)alertReceiverTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getDocumentRoot()
  {
    return documentRootEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getDocumentRoot_Mixed()
  {
    return (EAttribute)documentRootEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_XMLNSPrefixMap()
  {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_XSISchemaLocation()
  {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getDocumentRoot_SiteChecker()
  {
    return (EReference)documentRootEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getErrorPageList()
  {
    return errorPageListEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getErrorPageList_ErrorPage()
  {
    return (EAttribute)errorPageListEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSiteCheckerType()
  {
    return siteCheckerTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSiteCheckerType_SmtpProvider()
  {
    return (EReference)siteCheckerTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSiteCheckerType_Site()
  {
    return (EReference)siteCheckerTypeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSiteCheckerType1()
  {
    return siteCheckerType1EClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSiteCheckerType1_SmtpProvider()
  {
    return (EReference)siteCheckerType1EClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSiteCheckerType1_Site()
  {
    return (EReference)siteCheckerType1EClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSiteType()
  {
    return siteTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSiteType_URLPath()
  {
    return (EReference)siteTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSiteType_CheckInterval()
  {
    return (EAttribute)siteTypeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSiteType_ResponseTimeThreshold()
  {
    return (EAttribute)siteTypeEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSiteType_UnableToConnectRetryAttempts()
  {
    return (EAttribute)siteTypeEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSiteType_AlertList()
  {
    return (EReference)siteTypeEClass.getEStructuralFeatures().get(4);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSiteType_AlertInterval()
  {
    return (EAttribute)siteTypeEClass.getEStructuralFeatures().get(5);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSiteType_LogFile()
  {
    return (EAttribute)siteTypeEClass.getEStructuralFeatures().get(6);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSiteType_LogLevel()
  {
    return (EAttribute)siteTypeEClass.getEStructuralFeatures().get(7);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSiteType_NotifyWhenUp()
  {
    return (EAttribute)siteTypeEClass.getEStructuralFeatures().get(8);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getSiteType_ErrorPages()
  {
    return (EReference)siteTypeEClass.getEStructuralFeatures().get(9);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getSmtpProviderType()
  {
    return smtpProviderTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSmtpProviderType_FromAddress()
  {
    return (EAttribute)smtpProviderTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getSmtpProviderType_Host()
  {
    return (EAttribute)smtpProviderTypeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getURLSequence()
  {
    return urlSequenceEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EReference getURLSequence_URL()
  {
    return (EReference)urlSequenceEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EClass getURLType()
  {
    return urlTypeEClass;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getURLType_URL()
  {
    return (EAttribute)urlTypeEClass.getEStructuralFeatures().get(0);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getURLType_Method()
  {
    return (EAttribute)urlTypeEClass.getEStructuralFeatures().get(1);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getURLType_User()
  {
    return (EAttribute)urlTypeEClass.getEStructuralFeatures().get(2);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public EAttribute getURLType_Pwd()
  {
    return (EAttribute)urlTypeEClass.getEStructuralFeatures().get(3);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SitecheckerschemaFactory getSitecheckerschemaFactory()
  {
    return (SitecheckerschemaFactory)getEFactoryInstance();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isCreated = false;

  /**
   * Creates the meta-model objects for the package.  This method is
   * guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void createPackageContents()
  {
    if (isCreated) return;
    isCreated = true;

    // Create classes and their features
    alertListTypeEClass = createEClass(ALERT_LIST_TYPE);
    createEAttribute(alertListTypeEClass, ALERT_LIST_TYPE__ALERT_RECEIVER_EMAIL);

    alertReceiverTypeEClass = createEClass(ALERT_RECEIVER_TYPE);
    createEAttribute(alertReceiverTypeEClass, ALERT_RECEIVER_TYPE__EMAIL_ADDRESS);

    documentRootEClass = createEClass(DOCUMENT_ROOT);
    createEAttribute(documentRootEClass, DOCUMENT_ROOT__MIXED);
    createEReference(documentRootEClass, DOCUMENT_ROOT__XMLNS_PREFIX_MAP);
    createEReference(documentRootEClass, DOCUMENT_ROOT__XSI_SCHEMA_LOCATION);
    createEReference(documentRootEClass, DOCUMENT_ROOT__SITE_CHECKER);

    errorPageListEClass = createEClass(ERROR_PAGE_LIST);
    createEAttribute(errorPageListEClass, ERROR_PAGE_LIST__ERROR_PAGE);

    siteCheckerTypeEClass = createEClass(SITE_CHECKER_TYPE);
    createEReference(siteCheckerTypeEClass, SITE_CHECKER_TYPE__SMTP_PROVIDER);
    createEReference(siteCheckerTypeEClass, SITE_CHECKER_TYPE__SITE);

    siteCheckerType1EClass = createEClass(SITE_CHECKER_TYPE1);
    createEReference(siteCheckerType1EClass, SITE_CHECKER_TYPE1__SMTP_PROVIDER);
    createEReference(siteCheckerType1EClass, SITE_CHECKER_TYPE1__SITE);

    siteTypeEClass = createEClass(SITE_TYPE);
    createEReference(siteTypeEClass, SITE_TYPE__URL_PATH);
    createEAttribute(siteTypeEClass, SITE_TYPE__CHECK_INTERVAL);
    createEAttribute(siteTypeEClass, SITE_TYPE__RESPONSE_TIME_THRESHOLD);
    createEAttribute(siteTypeEClass, SITE_TYPE__UNABLE_TO_CONNECT_RETRY_ATTEMPTS);
    createEReference(siteTypeEClass, SITE_TYPE__ALERT_LIST);
    createEAttribute(siteTypeEClass, SITE_TYPE__ALERT_INTERVAL);
    createEAttribute(siteTypeEClass, SITE_TYPE__LOG_FILE);
    createEAttribute(siteTypeEClass, SITE_TYPE__LOG_LEVEL);
    createEAttribute(siteTypeEClass, SITE_TYPE__NOTIFY_WHEN_UP);
    createEReference(siteTypeEClass, SITE_TYPE__ERROR_PAGES);

    smtpProviderTypeEClass = createEClass(SMTP_PROVIDER_TYPE);
    createEAttribute(smtpProviderTypeEClass, SMTP_PROVIDER_TYPE__FROM_ADDRESS);
    createEAttribute(smtpProviderTypeEClass, SMTP_PROVIDER_TYPE__HOST);

    urlSequenceEClass = createEClass(URL_SEQUENCE);
    createEReference(urlSequenceEClass, URL_SEQUENCE__URL);

    urlTypeEClass = createEClass(URL_TYPE);
    createEAttribute(urlTypeEClass, URL_TYPE__URL);
    createEAttribute(urlTypeEClass, URL_TYPE__METHOD);
    createEAttribute(urlTypeEClass, URL_TYPE__USER);
    createEAttribute(urlTypeEClass, URL_TYPE__PWD);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private boolean isInitialized = false;

  /**
   * Complete the initialization of the package and its meta-model.  This
   * method is guarded to have no affect on any invocation but its first.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void initializePackageContents()
  {
    if (isInitialized) return;
    isInitialized = true;

    // Initialize package
    setName(eNAME);
    setNsPrefix(eNS_PREFIX);
    setNsURI(eNS_URI);

    // Obtain other dependent packages
    XMLTypePackage theXMLTypePackage = (XMLTypePackage)EPackage.Registry.INSTANCE.getEPackage(XMLTypePackage.eNS_URI);

    // Create type parameters

    // Set bounds for type parameters

    // Add supertypes to classes

    // Initialize classes and features; add operations and parameters
    initEClass(alertListTypeEClass, AlertListType.class, "AlertListType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getAlertListType_AlertReceiverEmail(), theXMLTypePackage.getString(), "alertReceiverEmail", null, 1, -1, AlertListType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(alertReceiverTypeEClass, AlertReceiverType.class, "AlertReceiverType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getAlertReceiverType_EmailAddress(), theXMLTypePackage.getString(), "emailAddress", null, 1, 1, AlertReceiverType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(documentRootEClass, DocumentRoot.class, "DocumentRoot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getDocumentRoot_Mixed(), ecorePackage.getEFeatureMapEntry(), "mixed", null, 0, -1, null, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_XMLNSPrefixMap(), ecorePackage.getEStringToStringMapEntry(), null, "xMLNSPrefixMap", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_XSISchemaLocation(), ecorePackage.getEStringToStringMapEntry(), null, "xSISchemaLocation", null, 0, -1, null, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getDocumentRoot_SiteChecker(), this.getSiteCheckerType1(), null, "siteChecker", null, 0, -2, null, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

    initEClass(errorPageListEClass, ErrorPageList.class, "ErrorPageList", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getErrorPageList_ErrorPage(), theXMLTypePackage.getString(), "errorPage", null, 0, -1, ErrorPageList.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(siteCheckerTypeEClass, SiteCheckerType.class, "SiteCheckerType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSiteCheckerType_SmtpProvider(), this.getSmtpProviderType(), null, "smtpProvider", null, 0, 1, SiteCheckerType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSiteCheckerType_Site(), this.getSiteType(), null, "site", null, 0, 10, SiteCheckerType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(siteCheckerType1EClass, SiteCheckerType1.class, "SiteCheckerType1", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSiteCheckerType1_SmtpProvider(), this.getSmtpProviderType(), null, "smtpProvider", null, 1, 1, SiteCheckerType1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSiteCheckerType1_Site(), this.getSiteType(), null, "site", null, 1, -1, SiteCheckerType1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(siteTypeEClass, SiteType.class, "SiteType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getSiteType_URLPath(), this.getURLSequence(), null, "uRLPath", null, 1, 1, SiteType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getSiteType_CheckInterval(), theXMLTypePackage.getPositiveInteger(), "checkInterval", "300", 1, 1, SiteType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getSiteType_ResponseTimeThreshold(), theXMLTypePackage.getPositiveInteger(), "responseTimeThreshold", "60", 1, 1, SiteType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getSiteType_UnableToConnectRetryAttempts(), theXMLTypePackage.getInt(), "unableToConnectRetryAttempts", "30", 1, 1, SiteType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSiteType_AlertList(), this.getAlertListType(), null, "alertList", null, 1, 1, SiteType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getSiteType_AlertInterval(), theXMLTypePackage.getPositiveInteger(), "alertInterval", "10", 1, 1, SiteType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getSiteType_LogFile(), theXMLTypePackage.getString(), "logFile", null, 1, 1, SiteType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getSiteType_LogLevel(), theXMLTypePackage.getString(), "logLevel", "INFO", 1, 1, SiteType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getSiteType_NotifyWhenUp(), theXMLTypePackage.getBoolean(), "notifyWhenUp", "true", 1, 1, SiteType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEReference(getSiteType_ErrorPages(), this.getErrorPageList(), null, "errorPages", null, 1, 1, SiteType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(smtpProviderTypeEClass, SmtpProviderType.class, "SmtpProviderType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getSmtpProviderType_FromAddress(), theXMLTypePackage.getString(), "fromAddress", "aeast@usatoday.com", 0, 1, SmtpProviderType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getSmtpProviderType_Host(), theXMLTypePackage.getString(), "host", null, 1, 1, SmtpProviderType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(urlSequenceEClass, URLSequence.class, "URLSequence", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEReference(getURLSequence_URL(), this.getURLType(), null, "uRL", null, 1, -1, URLSequence.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    initEClass(urlTypeEClass, URLType.class, "URLType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
    initEAttribute(getURLType_URL(), theXMLTypePackage.getString(), "uRL", "protocol://host:port/file", 1, 1, URLType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getURLType_Method(), theXMLTypePackage.getString(), "method", "GET", 1, 1, URLType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getURLType_User(), theXMLTypePackage.getString(), "user", null, 0, 1, URLType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
    initEAttribute(getURLType_Pwd(), theXMLTypePackage.getString(), "pwd", null, 0, 1, URLType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

    // Create resource
    createResource(eNS_URI);

    // Create annotations
    // http:///org/eclipse/emf/ecore/util/ExtendedMetaData
    createExtendedMetaDataAnnotations();
  }

  /**
   * Initializes the annotations for <b>http:///org/eclipse/emf/ecore/util/ExtendedMetaData</b>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected void createExtendedMetaDataAnnotations()
  {
    String source = "http:///org/eclipse/emf/ecore/util/ExtendedMetaData";			
    addAnnotation
      (alertListTypeEClass, 
       source, 
       new String[] 
       {
       "name", "AlertListType",
       "kind", "elementOnly"
       });		
    addAnnotation
      (getAlertListType_AlertReceiverEmail(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "AlertReceiverEmail"
       });		
    addAnnotation
      (alertReceiverTypeEClass, 
       source, 
       new String[] 
       {
       "name", "AlertReceiverType",
       "kind", "elementOnly"
       });		
    addAnnotation
      (getAlertReceiverType_EmailAddress(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "EmailAddress"
       });		
    addAnnotation
      (documentRootEClass, 
       source, 
       new String[] 
       {
       "name", "",
       "kind", "mixed"
       });		
    addAnnotation
      (getDocumentRoot_Mixed(), 
       source, 
       new String[] 
       {
       "kind", "elementWildcard",
       "name", ":mixed"
       });		
    addAnnotation
      (getDocumentRoot_XMLNSPrefixMap(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "xmlns:prefix"
       });		
    addAnnotation
      (getDocumentRoot_XSISchemaLocation(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "xsi:schemaLocation"
       });		
    addAnnotation
      (getDocumentRoot_SiteChecker(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "SiteChecker",
       "namespace", "##targetNamespace"
       });			
    addAnnotation
      (errorPageListEClass, 
       source, 
       new String[] 
       {
       "name", "ErrorPageList",
       "kind", "elementOnly"
       });		
    addAnnotation
      (getErrorPageList_ErrorPage(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "ErrorPage"
       });		
    addAnnotation
      (siteCheckerTypeEClass, 
       source, 
       new String[] 
       {
       "name", "SiteCheckerType",
       "kind", "elementOnly"
       });		
    addAnnotation
      (getSiteCheckerType_SmtpProvider(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "SmtpProvider"
       });		
    addAnnotation
      (getSiteCheckerType_Site(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "Site"
       });		
    addAnnotation
      (siteCheckerType1EClass, 
       source, 
       new String[] 
       {
       "name", "SiteChecker_._type",
       "kind", "elementOnly"
       });		
    addAnnotation
      (getSiteCheckerType1_SmtpProvider(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "SmtpProvider"
       });		
    addAnnotation
      (getSiteCheckerType1_Site(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "Site"
       });		
    addAnnotation
      (siteTypeEClass, 
       source, 
       new String[] 
       {
       "name", "SiteType",
       "kind", "elementOnly"
       });		
    addAnnotation
      (getSiteType_URLPath(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "URLPath"
       });		
    addAnnotation
      (getSiteType_CheckInterval(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "CheckInterval"
       });		
    addAnnotation
      (getSiteType_ResponseTimeThreshold(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "ResponseTimeThreshold"
       });		
    addAnnotation
      (getSiteType_UnableToConnectRetryAttempts(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "UnableToConnectRetryAttempts"
       });		
    addAnnotation
      (getSiteType_AlertList(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "AlertList"
       });		
    addAnnotation
      (getSiteType_AlertInterval(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "AlertInterval"
       });		
    addAnnotation
      (getSiteType_LogFile(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "LogFile"
       });		
    addAnnotation
      (getSiteType_LogLevel(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "LogLevel"
       });		
    addAnnotation
      (getSiteType_NotifyWhenUp(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "NotifyWhenUp"
       });		
    addAnnotation
      (getSiteType_ErrorPages(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "ErrorPages"
       });		
    addAnnotation
      (smtpProviderTypeEClass, 
       source, 
       new String[] 
       {
       "name", "SmtpProviderType",
       "kind", "empty"
       });		
    addAnnotation
      (getSmtpProviderType_FromAddress(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "fromAddress"
       });		
    addAnnotation
      (getSmtpProviderType_Host(), 
       source, 
       new String[] 
       {
       "kind", "attribute",
       "name", "host"
       });		
    addAnnotation
      (urlSequenceEClass, 
       source, 
       new String[] 
       {
       "name", "URLSequence",
       "kind", "elementOnly"
       });		
    addAnnotation
      (getURLSequence_URL(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "URL"
       });		
    addAnnotation
      (urlTypeEClass, 
       source, 
       new String[] 
       {
       "name", "URLType",
       "kind", "elementOnly"
       });			
    addAnnotation
      (getURLType_URL(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "URL"
       });		
    addAnnotation
      (getURLType_Method(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "Method"
       });		
    addAnnotation
      (getURLType_User(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "user"
       });		
    addAnnotation
      (getURLType_Pwd(), 
       source, 
       new String[] 
       {
       "kind", "element",
       "name", "pwd"
       });
  }

} //SitecheckerschemaPackageImpl
