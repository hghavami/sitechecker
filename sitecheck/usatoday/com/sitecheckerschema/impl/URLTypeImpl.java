/**
 * <copyright>
 * </copyright>
 *
 * $Id: URLTypeImpl.java,v 1.1 2011/01/17 20:25:51 aeast Exp $
 */
package sitecheck.usatoday.com.sitecheckerschema.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.sdo.impl.EDataObjectImpl;

import sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage;
import sitecheck.usatoday.com.sitecheckerschema.URLType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>URL Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.impl.URLTypeImpl#getURL <em>URL</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.impl.URLTypeImpl#getMethod <em>Method</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.impl.URLTypeImpl#getUser <em>User</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.impl.URLTypeImpl#getPwd <em>Pwd</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class URLTypeImpl extends EDataObjectImpl implements URLType
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static final long serialVersionUID = 1L;

  /**
   * The default value of the '{@link #getURL() <em>URL</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getURL()
   * @generated
   * @ordered
   */
  protected static final String URL_EDEFAULT = "protocol://host:port/file";

  /**
   * The cached value of the '{@link #getURL() <em>URL</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getURL()
   * @generated
   * @ordered
   */
  protected String uRL = URL_EDEFAULT;

  /**
   * This is true if the URL attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean uRLESet;

  /**
   * The default value of the '{@link #getMethod() <em>Method</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMethod()
   * @generated
   * @ordered
   */
  protected static final String METHOD_EDEFAULT = "GET";

  /**
   * The cached value of the '{@link #getMethod() <em>Method</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getMethod()
   * @generated
   * @ordered
   */
  protected String method = METHOD_EDEFAULT;

  /**
   * This is true if the Method attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean methodESet;

  /**
   * The default value of the '{@link #getUser() <em>User</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUser()
   * @generated
   * @ordered
   */
  protected static final String USER_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getUser() <em>User</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUser()
   * @generated
   * @ordered
   */
  protected String user = USER_EDEFAULT;

  /**
   * This is true if the User attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean userESet;

  /**
   * The default value of the '{@link #getPwd() <em>Pwd</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPwd()
   * @generated
   * @ordered
   */
  protected static final String PWD_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getPwd() <em>Pwd</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getPwd()
   * @generated
   * @ordered
   */
  protected String pwd = PWD_EDEFAULT;

  /**
   * This is true if the Pwd attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean pwdESet;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected URLTypeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SitecheckerschemaPackage.Literals.URL_TYPE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getURL()
  {
    return uRL;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setURL(String newURL)
  {
    String oldURL = uRL;
    uRL = newURL;
    boolean oldURLESet = uRLESet;
    uRLESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SitecheckerschemaPackage.URL_TYPE__URL, oldURL, uRL, !oldURLESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetURL()
  {
    String oldURL = uRL;
    boolean oldURLESet = uRLESet;
    uRL = URL_EDEFAULT;
    uRLESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SitecheckerschemaPackage.URL_TYPE__URL, oldURL, URL_EDEFAULT, oldURLESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetURL()
  {
    return uRLESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getMethod()
  {
    return method;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setMethod(String newMethod)
  {
    String oldMethod = method;
    method = newMethod;
    boolean oldMethodESet = methodESet;
    methodESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SitecheckerschemaPackage.URL_TYPE__METHOD, oldMethod, method, !oldMethodESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetMethod()
  {
    String oldMethod = method;
    boolean oldMethodESet = methodESet;
    method = METHOD_EDEFAULT;
    methodESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SitecheckerschemaPackage.URL_TYPE__METHOD, oldMethod, METHOD_EDEFAULT, oldMethodESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetMethod()
  {
    return methodESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getUser()
  {
    return user;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUser(String newUser)
  {
    String oldUser = user;
    user = newUser;
    boolean oldUserESet = userESet;
    userESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SitecheckerschemaPackage.URL_TYPE__USER, oldUser, user, !oldUserESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetUser()
  {
    String oldUser = user;
    boolean oldUserESet = userESet;
    user = USER_EDEFAULT;
    userESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SitecheckerschemaPackage.URL_TYPE__USER, oldUser, USER_EDEFAULT, oldUserESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetUser()
  {
    return userESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getPwd()
  {
    return pwd;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setPwd(String newPwd)
  {
    String oldPwd = pwd;
    pwd = newPwd;
    boolean oldPwdESet = pwdESet;
    pwdESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SitecheckerschemaPackage.URL_TYPE__PWD, oldPwd, pwd, !oldPwdESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetPwd()
  {
    String oldPwd = pwd;
    boolean oldPwdESet = pwdESet;
    pwd = PWD_EDEFAULT;
    pwdESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SitecheckerschemaPackage.URL_TYPE__PWD, oldPwd, PWD_EDEFAULT, oldPwdESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetPwd()
  {
    return pwdESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.URL_TYPE__URL:
        return getURL();
      case SitecheckerschemaPackage.URL_TYPE__METHOD:
        return getMethod();
      case SitecheckerschemaPackage.URL_TYPE__USER:
        return getUser();
      case SitecheckerschemaPackage.URL_TYPE__PWD:
        return getPwd();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.URL_TYPE__URL:
        setURL((String)newValue);
        return;
      case SitecheckerschemaPackage.URL_TYPE__METHOD:
        setMethod((String)newValue);
        return;
      case SitecheckerschemaPackage.URL_TYPE__USER:
        setUser((String)newValue);
        return;
      case SitecheckerschemaPackage.URL_TYPE__PWD:
        setPwd((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.URL_TYPE__URL:
        unsetURL();
        return;
      case SitecheckerschemaPackage.URL_TYPE__METHOD:
        unsetMethod();
        return;
      case SitecheckerschemaPackage.URL_TYPE__USER:
        unsetUser();
        return;
      case SitecheckerschemaPackage.URL_TYPE__PWD:
        unsetPwd();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.URL_TYPE__URL:
        return isSetURL();
      case SitecheckerschemaPackage.URL_TYPE__METHOD:
        return isSetMethod();
      case SitecheckerschemaPackage.URL_TYPE__USER:
        return isSetUser();
      case SitecheckerschemaPackage.URL_TYPE__PWD:
        return isSetPwd();
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (uRL: ");
    if (uRLESet) result.append(uRL); else result.append("<unset>");
    result.append(", method: ");
    if (methodESet) result.append(method); else result.append("<unset>");
    result.append(", user: ");
    if (userESet) result.append(user); else result.append("<unset>");
    result.append(", pwd: ");
    if (pwdESet) result.append(pwd); else result.append("<unset>");
    result.append(')');
    return result.toString();
  }

} //URLTypeImpl
