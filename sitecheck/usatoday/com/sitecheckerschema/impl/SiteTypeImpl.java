/**
 * <copyright>
 * </copyright>
 *
 * $Id: SiteTypeImpl.java,v 1.1 2011/01/17 20:25:51 aeast Exp $
 */
package sitecheck.usatoday.com.sitecheckerschema.impl;

import java.math.BigInteger;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.sdo.impl.EDataObjectImpl;

import sitecheck.usatoday.com.sitecheckerschema.AlertListType;
import sitecheck.usatoday.com.sitecheckerschema.ErrorPageList;
import sitecheck.usatoday.com.sitecheckerschema.SiteType;
import sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage;
import sitecheck.usatoday.com.sitecheckerschema.URLSequence;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Site Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.impl.SiteTypeImpl#getURLPath <em>URL Path</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.impl.SiteTypeImpl#getCheckInterval <em>Check Interval</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.impl.SiteTypeImpl#getResponseTimeThreshold <em>Response Time Threshold</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.impl.SiteTypeImpl#getUnableToConnectRetryAttempts <em>Unable To Connect Retry Attempts</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.impl.SiteTypeImpl#getAlertList <em>Alert List</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.impl.SiteTypeImpl#getAlertInterval <em>Alert Interval</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.impl.SiteTypeImpl#getLogFile <em>Log File</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.impl.SiteTypeImpl#getLogLevel <em>Log Level</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.impl.SiteTypeImpl#isNotifyWhenUp <em>Notify When Up</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.impl.SiteTypeImpl#getErrorPages <em>Error Pages</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SiteTypeImpl extends EDataObjectImpl implements SiteType
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static final long serialVersionUID = 1L;

  /**
   * The cached value of the '{@link #getURLPath() <em>URL Path</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getURLPath()
   * @generated
   * @ordered
   */
  protected URLSequence uRLPath;

  /**
   * The default value of the '{@link #getCheckInterval() <em>Check Interval</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCheckInterval()
   * @generated
   * @ordered
   */
  protected static final BigInteger CHECK_INTERVAL_EDEFAULT = new BigInteger("300");

  /**
   * The cached value of the '{@link #getCheckInterval() <em>Check Interval</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getCheckInterval()
   * @generated
   * @ordered
   */
  protected BigInteger checkInterval = CHECK_INTERVAL_EDEFAULT;

  /**
   * This is true if the Check Interval attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean checkIntervalESet;

  /**
   * The default value of the '{@link #getResponseTimeThreshold() <em>Response Time Threshold</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getResponseTimeThreshold()
   * @generated
   * @ordered
   */
  protected static final BigInteger RESPONSE_TIME_THRESHOLD_EDEFAULT = new BigInteger("60");

  /**
   * The cached value of the '{@link #getResponseTimeThreshold() <em>Response Time Threshold</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getResponseTimeThreshold()
   * @generated
   * @ordered
   */
  protected BigInteger responseTimeThreshold = RESPONSE_TIME_THRESHOLD_EDEFAULT;

  /**
   * This is true if the Response Time Threshold attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean responseTimeThresholdESet;

  /**
   * The default value of the '{@link #getUnableToConnectRetryAttempts() <em>Unable To Connect Retry Attempts</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUnableToConnectRetryAttempts()
   * @generated
   * @ordered
   */
  protected static final int UNABLE_TO_CONNECT_RETRY_ATTEMPTS_EDEFAULT = 30;

  /**
   * The cached value of the '{@link #getUnableToConnectRetryAttempts() <em>Unable To Connect Retry Attempts</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getUnableToConnectRetryAttempts()
   * @generated
   * @ordered
   */
  protected int unableToConnectRetryAttempts = UNABLE_TO_CONNECT_RETRY_ATTEMPTS_EDEFAULT;

  /**
   * This is true if the Unable To Connect Retry Attempts attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean unableToConnectRetryAttemptsESet;

  /**
   * The cached value of the '{@link #getAlertList() <em>Alert List</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAlertList()
   * @generated
   * @ordered
   */
  protected AlertListType alertList;

  /**
   * The default value of the '{@link #getAlertInterval() <em>Alert Interval</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAlertInterval()
   * @generated
   * @ordered
   */
  protected static final BigInteger ALERT_INTERVAL_EDEFAULT = new BigInteger("10");

  /**
   * The cached value of the '{@link #getAlertInterval() <em>Alert Interval</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getAlertInterval()
   * @generated
   * @ordered
   */
  protected BigInteger alertInterval = ALERT_INTERVAL_EDEFAULT;

  /**
   * This is true if the Alert Interval attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean alertIntervalESet;

  /**
   * The default value of the '{@link #getLogFile() <em>Log File</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLogFile()
   * @generated
   * @ordered
   */
  protected static final String LOG_FILE_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getLogFile() <em>Log File</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLogFile()
   * @generated
   * @ordered
   */
  protected String logFile = LOG_FILE_EDEFAULT;

  /**
   * This is true if the Log File attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean logFileESet;

  /**
   * The default value of the '{@link #getLogLevel() <em>Log Level</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLogLevel()
   * @generated
   * @ordered
   */
  protected static final String LOG_LEVEL_EDEFAULT = "INFO";

  /**
   * The cached value of the '{@link #getLogLevel() <em>Log Level</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getLogLevel()
   * @generated
   * @ordered
   */
  protected String logLevel = LOG_LEVEL_EDEFAULT;

  /**
   * This is true if the Log Level attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean logLevelESet;

  /**
   * The default value of the '{@link #isNotifyWhenUp() <em>Notify When Up</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isNotifyWhenUp()
   * @generated
   * @ordered
   */
  protected static final boolean NOTIFY_WHEN_UP_EDEFAULT = true;

  /**
   * The cached value of the '{@link #isNotifyWhenUp() <em>Notify When Up</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isNotifyWhenUp()
   * @generated
   * @ordered
   */
  protected boolean notifyWhenUp = NOTIFY_WHEN_UP_EDEFAULT;

  /**
   * This is true if the Notify When Up attribute has been set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  protected boolean notifyWhenUpESet;

  /**
   * The cached value of the '{@link #getErrorPages() <em>Error Pages</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getErrorPages()
   * @generated
   * @ordered
   */
  protected ErrorPageList errorPages;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SiteTypeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SitecheckerschemaPackage.Literals.SITE_TYPE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public URLSequence getURLPath()
  {
    return uRLPath;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetURLPath(URLSequence newURLPath, NotificationChain msgs)
  {
    URLSequence oldURLPath = uRLPath;
    uRLPath = newURLPath;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SitecheckerschemaPackage.SITE_TYPE__URL_PATH, oldURLPath, newURLPath);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setURLPath(URLSequence newURLPath)
  {
    if (newURLPath != uRLPath)
    {
      NotificationChain msgs = null;
      if (uRLPath != null)
        msgs = ((InternalEObject)uRLPath).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SitecheckerschemaPackage.SITE_TYPE__URL_PATH, null, msgs);
      if (newURLPath != null)
        msgs = ((InternalEObject)newURLPath).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SitecheckerschemaPackage.SITE_TYPE__URL_PATH, null, msgs);
      msgs = basicSetURLPath(newURLPath, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SitecheckerschemaPackage.SITE_TYPE__URL_PATH, newURLPath, newURLPath));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BigInteger getCheckInterval()
  {
    return checkInterval;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setCheckInterval(BigInteger newCheckInterval)
  {
    BigInteger oldCheckInterval = checkInterval;
    checkInterval = newCheckInterval;
    boolean oldCheckIntervalESet = checkIntervalESet;
    checkIntervalESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SitecheckerschemaPackage.SITE_TYPE__CHECK_INTERVAL, oldCheckInterval, checkInterval, !oldCheckIntervalESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetCheckInterval()
  {
    BigInteger oldCheckInterval = checkInterval;
    boolean oldCheckIntervalESet = checkIntervalESet;
    checkInterval = CHECK_INTERVAL_EDEFAULT;
    checkIntervalESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SitecheckerschemaPackage.SITE_TYPE__CHECK_INTERVAL, oldCheckInterval, CHECK_INTERVAL_EDEFAULT, oldCheckIntervalESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetCheckInterval()
  {
    return checkIntervalESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BigInteger getResponseTimeThreshold()
  {
    return responseTimeThreshold;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setResponseTimeThreshold(BigInteger newResponseTimeThreshold)
  {
    BigInteger oldResponseTimeThreshold = responseTimeThreshold;
    responseTimeThreshold = newResponseTimeThreshold;
    boolean oldResponseTimeThresholdESet = responseTimeThresholdESet;
    responseTimeThresholdESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SitecheckerschemaPackage.SITE_TYPE__RESPONSE_TIME_THRESHOLD, oldResponseTimeThreshold, responseTimeThreshold, !oldResponseTimeThresholdESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetResponseTimeThreshold()
  {
    BigInteger oldResponseTimeThreshold = responseTimeThreshold;
    boolean oldResponseTimeThresholdESet = responseTimeThresholdESet;
    responseTimeThreshold = RESPONSE_TIME_THRESHOLD_EDEFAULT;
    responseTimeThresholdESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SitecheckerschemaPackage.SITE_TYPE__RESPONSE_TIME_THRESHOLD, oldResponseTimeThreshold, RESPONSE_TIME_THRESHOLD_EDEFAULT, oldResponseTimeThresholdESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetResponseTimeThreshold()
  {
    return responseTimeThresholdESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public int getUnableToConnectRetryAttempts()
  {
    return unableToConnectRetryAttempts;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setUnableToConnectRetryAttempts(int newUnableToConnectRetryAttempts)
  {
    int oldUnableToConnectRetryAttempts = unableToConnectRetryAttempts;
    unableToConnectRetryAttempts = newUnableToConnectRetryAttempts;
    boolean oldUnableToConnectRetryAttemptsESet = unableToConnectRetryAttemptsESet;
    unableToConnectRetryAttemptsESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SitecheckerschemaPackage.SITE_TYPE__UNABLE_TO_CONNECT_RETRY_ATTEMPTS, oldUnableToConnectRetryAttempts, unableToConnectRetryAttempts, !oldUnableToConnectRetryAttemptsESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetUnableToConnectRetryAttempts()
  {
    int oldUnableToConnectRetryAttempts = unableToConnectRetryAttempts;
    boolean oldUnableToConnectRetryAttemptsESet = unableToConnectRetryAttemptsESet;
    unableToConnectRetryAttempts = UNABLE_TO_CONNECT_RETRY_ATTEMPTS_EDEFAULT;
    unableToConnectRetryAttemptsESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SitecheckerschemaPackage.SITE_TYPE__UNABLE_TO_CONNECT_RETRY_ATTEMPTS, oldUnableToConnectRetryAttempts, UNABLE_TO_CONNECT_RETRY_ATTEMPTS_EDEFAULT, oldUnableToConnectRetryAttemptsESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetUnableToConnectRetryAttempts()
  {
    return unableToConnectRetryAttemptsESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public AlertListType getAlertList()
  {
    return alertList;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetAlertList(AlertListType newAlertList, NotificationChain msgs)
  {
    AlertListType oldAlertList = alertList;
    alertList = newAlertList;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SitecheckerschemaPackage.SITE_TYPE__ALERT_LIST, oldAlertList, newAlertList);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAlertList(AlertListType newAlertList)
  {
    if (newAlertList != alertList)
    {
      NotificationChain msgs = null;
      if (alertList != null)
        msgs = ((InternalEObject)alertList).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SitecheckerschemaPackage.SITE_TYPE__ALERT_LIST, null, msgs);
      if (newAlertList != null)
        msgs = ((InternalEObject)newAlertList).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SitecheckerschemaPackage.SITE_TYPE__ALERT_LIST, null, msgs);
      msgs = basicSetAlertList(newAlertList, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SitecheckerschemaPackage.SITE_TYPE__ALERT_LIST, newAlertList, newAlertList));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public BigInteger getAlertInterval()
  {
    return alertInterval;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setAlertInterval(BigInteger newAlertInterval)
  {
    BigInteger oldAlertInterval = alertInterval;
    alertInterval = newAlertInterval;
    boolean oldAlertIntervalESet = alertIntervalESet;
    alertIntervalESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SitecheckerschemaPackage.SITE_TYPE__ALERT_INTERVAL, oldAlertInterval, alertInterval, !oldAlertIntervalESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetAlertInterval()
  {
    BigInteger oldAlertInterval = alertInterval;
    boolean oldAlertIntervalESet = alertIntervalESet;
    alertInterval = ALERT_INTERVAL_EDEFAULT;
    alertIntervalESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SitecheckerschemaPackage.SITE_TYPE__ALERT_INTERVAL, oldAlertInterval, ALERT_INTERVAL_EDEFAULT, oldAlertIntervalESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetAlertInterval()
  {
    return alertIntervalESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLogFile()
  {
    return logFile;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLogFile(String newLogFile)
  {
    String oldLogFile = logFile;
    logFile = newLogFile;
    boolean oldLogFileESet = logFileESet;
    logFileESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SitecheckerschemaPackage.SITE_TYPE__LOG_FILE, oldLogFile, logFile, !oldLogFileESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetLogFile()
  {
    String oldLogFile = logFile;
    boolean oldLogFileESet = logFileESet;
    logFile = LOG_FILE_EDEFAULT;
    logFileESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SitecheckerschemaPackage.SITE_TYPE__LOG_FILE, oldLogFile, LOG_FILE_EDEFAULT, oldLogFileESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetLogFile()
  {
    return logFileESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getLogLevel()
  {
    return logLevel;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setLogLevel(String newLogLevel)
  {
    String oldLogLevel = logLevel;
    logLevel = newLogLevel;
    boolean oldLogLevelESet = logLevelESet;
    logLevelESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SitecheckerschemaPackage.SITE_TYPE__LOG_LEVEL, oldLogLevel, logLevel, !oldLogLevelESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetLogLevel()
  {
    String oldLogLevel = logLevel;
    boolean oldLogLevelESet = logLevelESet;
    logLevel = LOG_LEVEL_EDEFAULT;
    logLevelESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SitecheckerschemaPackage.SITE_TYPE__LOG_LEVEL, oldLogLevel, LOG_LEVEL_EDEFAULT, oldLogLevelESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetLogLevel()
  {
    return logLevelESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isNotifyWhenUp()
  {
    return notifyWhenUp;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setNotifyWhenUp(boolean newNotifyWhenUp)
  {
    boolean oldNotifyWhenUp = notifyWhenUp;
    notifyWhenUp = newNotifyWhenUp;
    boolean oldNotifyWhenUpESet = notifyWhenUpESet;
    notifyWhenUpESet = true;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SitecheckerschemaPackage.SITE_TYPE__NOTIFY_WHEN_UP, oldNotifyWhenUp, notifyWhenUp, !oldNotifyWhenUpESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void unsetNotifyWhenUp()
  {
    boolean oldNotifyWhenUp = notifyWhenUp;
    boolean oldNotifyWhenUpESet = notifyWhenUpESet;
    notifyWhenUp = NOTIFY_WHEN_UP_EDEFAULT;
    notifyWhenUpESet = false;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.UNSET, SitecheckerschemaPackage.SITE_TYPE__NOTIFY_WHEN_UP, oldNotifyWhenUp, NOTIFY_WHEN_UP_EDEFAULT, oldNotifyWhenUpESet));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public boolean isSetNotifyWhenUp()
  {
    return notifyWhenUpESet;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public ErrorPageList getErrorPages()
  {
    return errorPages;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetErrorPages(ErrorPageList newErrorPages, NotificationChain msgs)
  {
    ErrorPageList oldErrorPages = errorPages;
    errorPages = newErrorPages;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SitecheckerschemaPackage.SITE_TYPE__ERROR_PAGES, oldErrorPages, newErrorPages);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setErrorPages(ErrorPageList newErrorPages)
  {
    if (newErrorPages != errorPages)
    {
      NotificationChain msgs = null;
      if (errorPages != null)
        msgs = ((InternalEObject)errorPages).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SitecheckerschemaPackage.SITE_TYPE__ERROR_PAGES, null, msgs);
      if (newErrorPages != null)
        msgs = ((InternalEObject)newErrorPages).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SitecheckerschemaPackage.SITE_TYPE__ERROR_PAGES, null, msgs);
      msgs = basicSetErrorPages(newErrorPages, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SitecheckerschemaPackage.SITE_TYPE__ERROR_PAGES, newErrorPages, newErrorPages));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.SITE_TYPE__URL_PATH:
        return basicSetURLPath(null, msgs);
      case SitecheckerschemaPackage.SITE_TYPE__ALERT_LIST:
        return basicSetAlertList(null, msgs);
      case SitecheckerschemaPackage.SITE_TYPE__ERROR_PAGES:
        return basicSetErrorPages(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.SITE_TYPE__URL_PATH:
        return getURLPath();
      case SitecheckerschemaPackage.SITE_TYPE__CHECK_INTERVAL:
        return getCheckInterval();
      case SitecheckerschemaPackage.SITE_TYPE__RESPONSE_TIME_THRESHOLD:
        return getResponseTimeThreshold();
      case SitecheckerschemaPackage.SITE_TYPE__UNABLE_TO_CONNECT_RETRY_ATTEMPTS:
        return new Integer(getUnableToConnectRetryAttempts());
      case SitecheckerschemaPackage.SITE_TYPE__ALERT_LIST:
        return getAlertList();
      case SitecheckerschemaPackage.SITE_TYPE__ALERT_INTERVAL:
        return getAlertInterval();
      case SitecheckerschemaPackage.SITE_TYPE__LOG_FILE:
        return getLogFile();
      case SitecheckerschemaPackage.SITE_TYPE__LOG_LEVEL:
        return getLogLevel();
      case SitecheckerschemaPackage.SITE_TYPE__NOTIFY_WHEN_UP:
        return isNotifyWhenUp() ? Boolean.TRUE : Boolean.FALSE;
      case SitecheckerschemaPackage.SITE_TYPE__ERROR_PAGES:
        return getErrorPages();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.SITE_TYPE__URL_PATH:
        setURLPath((URLSequence)newValue);
        return;
      case SitecheckerschemaPackage.SITE_TYPE__CHECK_INTERVAL:
        setCheckInterval((BigInteger)newValue);
        return;
      case SitecheckerschemaPackage.SITE_TYPE__RESPONSE_TIME_THRESHOLD:
        setResponseTimeThreshold((BigInteger)newValue);
        return;
      case SitecheckerschemaPackage.SITE_TYPE__UNABLE_TO_CONNECT_RETRY_ATTEMPTS:
        setUnableToConnectRetryAttempts(((Integer)newValue).intValue());
        return;
      case SitecheckerschemaPackage.SITE_TYPE__ALERT_LIST:
        setAlertList((AlertListType)newValue);
        return;
      case SitecheckerschemaPackage.SITE_TYPE__ALERT_INTERVAL:
        setAlertInterval((BigInteger)newValue);
        return;
      case SitecheckerschemaPackage.SITE_TYPE__LOG_FILE:
        setLogFile((String)newValue);
        return;
      case SitecheckerschemaPackage.SITE_TYPE__LOG_LEVEL:
        setLogLevel((String)newValue);
        return;
      case SitecheckerschemaPackage.SITE_TYPE__NOTIFY_WHEN_UP:
        setNotifyWhenUp(((Boolean)newValue).booleanValue());
        return;
      case SitecheckerschemaPackage.SITE_TYPE__ERROR_PAGES:
        setErrorPages((ErrorPageList)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.SITE_TYPE__URL_PATH:
        setURLPath((URLSequence)null);
        return;
      case SitecheckerschemaPackage.SITE_TYPE__CHECK_INTERVAL:
        unsetCheckInterval();
        return;
      case SitecheckerschemaPackage.SITE_TYPE__RESPONSE_TIME_THRESHOLD:
        unsetResponseTimeThreshold();
        return;
      case SitecheckerschemaPackage.SITE_TYPE__UNABLE_TO_CONNECT_RETRY_ATTEMPTS:
        unsetUnableToConnectRetryAttempts();
        return;
      case SitecheckerschemaPackage.SITE_TYPE__ALERT_LIST:
        setAlertList((AlertListType)null);
        return;
      case SitecheckerschemaPackage.SITE_TYPE__ALERT_INTERVAL:
        unsetAlertInterval();
        return;
      case SitecheckerschemaPackage.SITE_TYPE__LOG_FILE:
        unsetLogFile();
        return;
      case SitecheckerschemaPackage.SITE_TYPE__LOG_LEVEL:
        unsetLogLevel();
        return;
      case SitecheckerschemaPackage.SITE_TYPE__NOTIFY_WHEN_UP:
        unsetNotifyWhenUp();
        return;
      case SitecheckerschemaPackage.SITE_TYPE__ERROR_PAGES:
        setErrorPages((ErrorPageList)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.SITE_TYPE__URL_PATH:
        return uRLPath != null;
      case SitecheckerschemaPackage.SITE_TYPE__CHECK_INTERVAL:
        return isSetCheckInterval();
      case SitecheckerschemaPackage.SITE_TYPE__RESPONSE_TIME_THRESHOLD:
        return isSetResponseTimeThreshold();
      case SitecheckerschemaPackage.SITE_TYPE__UNABLE_TO_CONNECT_RETRY_ATTEMPTS:
        return isSetUnableToConnectRetryAttempts();
      case SitecheckerschemaPackage.SITE_TYPE__ALERT_LIST:
        return alertList != null;
      case SitecheckerschemaPackage.SITE_TYPE__ALERT_INTERVAL:
        return isSetAlertInterval();
      case SitecheckerschemaPackage.SITE_TYPE__LOG_FILE:
        return isSetLogFile();
      case SitecheckerschemaPackage.SITE_TYPE__LOG_LEVEL:
        return isSetLogLevel();
      case SitecheckerschemaPackage.SITE_TYPE__NOTIFY_WHEN_UP:
        return isSetNotifyWhenUp();
      case SitecheckerschemaPackage.SITE_TYPE__ERROR_PAGES:
        return errorPages != null;
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (checkInterval: ");
    if (checkIntervalESet) result.append(checkInterval); else result.append("<unset>");
    result.append(", responseTimeThreshold: ");
    if (responseTimeThresholdESet) result.append(responseTimeThreshold); else result.append("<unset>");
    result.append(", unableToConnectRetryAttempts: ");
    if (unableToConnectRetryAttemptsESet) result.append(unableToConnectRetryAttempts); else result.append("<unset>");
    result.append(", alertInterval: ");
    if (alertIntervalESet) result.append(alertInterval); else result.append("<unset>");
    result.append(", logFile: ");
    if (logFileESet) result.append(logFile); else result.append("<unset>");
    result.append(", logLevel: ");
    if (logLevelESet) result.append(logLevel); else result.append("<unset>");
    result.append(", notifyWhenUp: ");
    if (notifyWhenUpESet) result.append(notifyWhenUp); else result.append("<unset>");
    result.append(')');
    return result.toString();
  }

} //SiteTypeImpl
