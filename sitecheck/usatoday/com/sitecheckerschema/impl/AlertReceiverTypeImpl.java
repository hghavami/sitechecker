/**
 * <copyright>
 * </copyright>
 *
 * $Id: AlertReceiverTypeImpl.java,v 1.1 2011/01/17 20:25:51 aeast Exp $
 */
package sitecheck.usatoday.com.sitecheckerschema.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.sdo.impl.EDataObjectImpl;

import sitecheck.usatoday.com.sitecheckerschema.AlertReceiverType;
import sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Alert Receiver Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.impl.AlertReceiverTypeImpl#getEmailAddress <em>Email Address</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class AlertReceiverTypeImpl extends EDataObjectImpl implements AlertReceiverType
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static final long serialVersionUID = 1L;

  /**
   * The default value of the '{@link #getEmailAddress() <em>Email Address</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEmailAddress()
   * @generated
   * @ordered
   */
  protected static final String EMAIL_ADDRESS_EDEFAULT = null;

  /**
   * The cached value of the '{@link #getEmailAddress() <em>Email Address</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getEmailAddress()
   * @generated
   * @ordered
   */
  protected String emailAddress = EMAIL_ADDRESS_EDEFAULT;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AlertReceiverTypeImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SitecheckerschemaPackage.Literals.ALERT_RECEIVER_TYPE;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public String getEmailAddress()
  {
    return emailAddress;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setEmailAddress(String newEmailAddress)
  {
    String oldEmailAddress = emailAddress;
    emailAddress = newEmailAddress;
    if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SitecheckerschemaPackage.ALERT_RECEIVER_TYPE__EMAIL_ADDRESS, oldEmailAddress, emailAddress));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.ALERT_RECEIVER_TYPE__EMAIL_ADDRESS:
        return getEmailAddress();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.ALERT_RECEIVER_TYPE__EMAIL_ADDRESS:
        setEmailAddress((String)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.ALERT_RECEIVER_TYPE__EMAIL_ADDRESS:
        setEmailAddress(EMAIL_ADDRESS_EDEFAULT);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.ALERT_RECEIVER_TYPE__EMAIL_ADDRESS:
        return EMAIL_ADDRESS_EDEFAULT == null ? emailAddress != null : !EMAIL_ADDRESS_EDEFAULT.equals(emailAddress);
    }
    return super.eIsSet(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public String toString()
  {
    if (eIsProxy()) return super.toString();

    StringBuffer result = new StringBuffer(super.toString());
    result.append(" (emailAddress: ");
    result.append(emailAddress);
    result.append(')');
    return result.toString();
  }

} //AlertReceiverTypeImpl
