/**
 * <copyright>
 * </copyright>
 *
 * $Id: SiteCheckerType1Impl.java,v 1.1 2011/01/17 20:25:51 aeast Exp $
 */
package sitecheck.usatoday.com.sitecheckerschema.impl;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.sdo.impl.EDataObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType1;
import sitecheck.usatoday.com.sitecheckerschema.SiteType;
import sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage;
import sitecheck.usatoday.com.sitecheckerschema.SmtpProviderType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Site Checker Type1</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.impl.SiteCheckerType1Impl#getSmtpProvider <em>Smtp Provider</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.impl.SiteCheckerType1Impl#getSite <em>Site</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SiteCheckerType1Impl extends EDataObjectImpl implements SiteCheckerType1
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  private static final long serialVersionUID = 1L;

  /**
   * The cached value of the '{@link #getSmtpProvider() <em>Smtp Provider</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSmtpProvider()
   * @generated
   * @ordered
   */
  protected SmtpProviderType smtpProvider;

  /**
   * The cached value of the '{@link #getSite() <em>Site</em>}' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getSite()
   * @generated
   * @ordered
   */
  protected EList<SiteType> site;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SiteCheckerType1Impl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return SitecheckerschemaPackage.Literals.SITE_CHECKER_TYPE1;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SmtpProviderType getSmtpProvider()
  {
    return smtpProvider;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetSmtpProvider(SmtpProviderType newSmtpProvider, NotificationChain msgs)
  {
    SmtpProviderType oldSmtpProvider = smtpProvider;
    smtpProvider = newSmtpProvider;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, SitecheckerschemaPackage.SITE_CHECKER_TYPE1__SMTP_PROVIDER, oldSmtpProvider, newSmtpProvider);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setSmtpProvider(SmtpProviderType newSmtpProvider)
  {
    if (newSmtpProvider != smtpProvider)
    {
      NotificationChain msgs = null;
      if (smtpProvider != null)
        msgs = ((InternalEObject)smtpProvider).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - SitecheckerschemaPackage.SITE_CHECKER_TYPE1__SMTP_PROVIDER, null, msgs);
      if (newSmtpProvider != null)
        msgs = ((InternalEObject)newSmtpProvider).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SitecheckerschemaPackage.SITE_CHECKER_TYPE1__SMTP_PROVIDER, null, msgs);
      msgs = basicSetSmtpProvider(newSmtpProvider, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, SitecheckerschemaPackage.SITE_CHECKER_TYPE1__SMTP_PROVIDER, newSmtpProvider, newSmtpProvider));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
public List<SiteType> getSite()
  {
    if (site == null)
    {
      site = new EObjectContainmentEList<SiteType>(SiteType.class, this, SitecheckerschemaPackage.SITE_CHECKER_TYPE1__SITE);
    }
    return site;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.SITE_CHECKER_TYPE1__SMTP_PROVIDER:
        return basicSetSmtpProvider(null, msgs);
      case SitecheckerschemaPackage.SITE_CHECKER_TYPE1__SITE:
        return ((InternalEList<?>)getSite()).basicRemove(otherEnd, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.SITE_CHECKER_TYPE1__SMTP_PROVIDER:
        return getSmtpProvider();
      case SitecheckerschemaPackage.SITE_CHECKER_TYPE1__SITE:
        return getSite();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @SuppressWarnings("unchecked")
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.SITE_CHECKER_TYPE1__SMTP_PROVIDER:
        setSmtpProvider((SmtpProviderType)newValue);
        return;
      case SitecheckerschemaPackage.SITE_CHECKER_TYPE1__SITE:
        getSite().clear();
        getSite().addAll((Collection<? extends SiteType>)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.SITE_CHECKER_TYPE1__SMTP_PROVIDER:
        setSmtpProvider((SmtpProviderType)null);
        return;
      case SitecheckerschemaPackage.SITE_CHECKER_TYPE1__SITE:
        getSite().clear();
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case SitecheckerschemaPackage.SITE_CHECKER_TYPE1__SMTP_PROVIDER:
        return smtpProvider != null;
      case SitecheckerschemaPackage.SITE_CHECKER_TYPE1__SITE:
        return site != null && !site.isEmpty();
    }
    return super.eIsSet(featureID);
  }

} //SiteCheckerType1Impl
