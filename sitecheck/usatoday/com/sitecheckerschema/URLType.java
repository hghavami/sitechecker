/**
 * <copyright>
 * </copyright>
 *
 * $Id: URLType.java,v 1.1 2011/01/17 20:25:51 aeast Exp $
 */
package sitecheck.usatoday.com.sitecheckerschema;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>URL Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.URLType#getURL <em>URL</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.URLType#getMethod <em>Method</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.URLType#getUser <em>User</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.URLType#getPwd <em>Pwd</em>}</li>
 * </ul>
 * </p>
 *
 * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getURLType()
 * @model extendedMetaData="name='URLType' kind='elementOnly'"
 * @generated
 */
public interface URLType
{
  /**
   * Returns the value of the '<em><b>URL</b></em>' attribute.
   * The default value is <code>"protocol://host:port/file"</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * <!-- begin-model-doc -->
   * 
   * 						This program currently supports HTTP, HTTPS, and
   * 						FTP protocols
   * 					
   * <!-- end-model-doc -->
   * @return the value of the '<em>URL</em>' attribute.
   * @see #isSetURL()
   * @see #unsetURL()
   * @see #setURL(String)
   * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getURLType_URL()
   * @model default="protocol://host:port/file" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
   *        extendedMetaData="kind='element' name='URL'"
   * @generated
   */
  String getURL();

  /**
   * Sets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.URLType#getURL <em>URL</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>URL</em>' attribute.
   * @see #isSetURL()
   * @see #unsetURL()
   * @see #getURL()
   * @generated
   */
  void setURL(String value);

  /**
   * Unsets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.URLType#getURL <em>URL</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetURL()
   * @see #getURL()
   * @see #setURL(String)
   * @generated
   */
  void unsetURL();

  /**
   * Returns whether the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.URLType#getURL <em>URL</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>URL</em>' attribute is set.
   * @see #unsetURL()
   * @see #getURL()
   * @see #setURL(String)
   * @generated
   */
  boolean isSetURL();

  /**
   * Returns the value of the '<em><b>Method</b></em>' attribute.
   * The default value is <code>"GET"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Method</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Method</em>' attribute.
   * @see #isSetMethod()
   * @see #unsetMethod()
   * @see #setMethod(String)
   * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getURLType_Method()
   * @model default="GET" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
   *        extendedMetaData="kind='element' name='Method'"
   * @generated
   */
  String getMethod();

  /**
   * Sets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.URLType#getMethod <em>Method</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Method</em>' attribute.
   * @see #isSetMethod()
   * @see #unsetMethod()
   * @see #getMethod()
   * @generated
   */
  void setMethod(String value);

  /**
   * Unsets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.URLType#getMethod <em>Method</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetMethod()
   * @see #getMethod()
   * @see #setMethod(String)
   * @generated
   */
  void unsetMethod();

  /**
   * Returns whether the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.URLType#getMethod <em>Method</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Method</em>' attribute is set.
   * @see #unsetMethod()
   * @see #getMethod()
   * @see #setMethod(String)
   * @generated
   */
  boolean isSetMethod();

  /**
   * Returns the value of the '<em><b>User</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>User</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>User</em>' attribute.
   * @see #isSetUser()
   * @see #unsetUser()
   * @see #setUser(String)
   * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getURLType_User()
   * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.String"
   *        extendedMetaData="kind='element' name='user'"
   * @generated
   */
  String getUser();

  /**
   * Sets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.URLType#getUser <em>User</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>User</em>' attribute.
   * @see #isSetUser()
   * @see #unsetUser()
   * @see #getUser()
   * @generated
   */
  void setUser(String value);

  /**
   * Unsets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.URLType#getUser <em>User</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetUser()
   * @see #getUser()
   * @see #setUser(String)
   * @generated
   */
  void unsetUser();

  /**
   * Returns whether the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.URLType#getUser <em>User</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>User</em>' attribute is set.
   * @see #unsetUser()
   * @see #getUser()
   * @see #setUser(String)
   * @generated
   */
  boolean isSetUser();

  /**
   * Returns the value of the '<em><b>Pwd</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Pwd</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Pwd</em>' attribute.
   * @see #isSetPwd()
   * @see #unsetPwd()
   * @see #setPwd(String)
   * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getURLType_Pwd()
   * @model unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.String"
   *        extendedMetaData="kind='element' name='pwd'"
   * @generated
   */
  String getPwd();

  /**
   * Sets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.URLType#getPwd <em>Pwd</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Pwd</em>' attribute.
   * @see #isSetPwd()
   * @see #unsetPwd()
   * @see #getPwd()
   * @generated
   */
  void setPwd(String value);

  /**
   * Unsets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.URLType#getPwd <em>Pwd</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetPwd()
   * @see #getPwd()
   * @see #setPwd(String)
   * @generated
   */
  void unsetPwd();

  /**
   * Returns whether the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.URLType#getPwd <em>Pwd</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>Pwd</em>' attribute is set.
   * @see #unsetPwd()
   * @see #getPwd()
   * @see #setPwd(String)
   * @generated
   */
  boolean isSetPwd();

} // URLType
