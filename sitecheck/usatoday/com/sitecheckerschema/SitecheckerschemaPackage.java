/**
 * <copyright>
 * </copyright>
 *
 * $Id: SitecheckerschemaPackage.java,v 1.1 2011/01/17 20:25:51 aeast Exp $
 */
package sitecheck.usatoday.com.sitecheckerschema;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * <!-- begin-model-doc -->
 * 
 * 			Site Checker Schema For USA TODAY
 * 		
 * <!-- end-model-doc -->
 * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaFactory
 * @model kind="package"
 * @generated
 */
public interface SitecheckerschemaPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "sitecheckerschema";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://com.usatoday.sitecheck/sitecheckerschema";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "sitecheckerschema";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  SitecheckerschemaPackage eINSTANCE = sitecheck.usatoday.com.sitecheckerschema.impl.SitecheckerschemaPackageImpl.init();

  /**
   * The meta object id for the '{@link sitecheck.usatoday.com.sitecheckerschema.impl.AlertListTypeImpl <em>Alert List Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see sitecheck.usatoday.com.sitecheckerschema.impl.AlertListTypeImpl
   * @see sitecheck.usatoday.com.sitecheckerschema.impl.SitecheckerschemaPackageImpl#getAlertListType()
   * @generated
   */
  int ALERT_LIST_TYPE = 0;

  /**
   * The feature id for the '<em><b>Alert Receiver Email</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALERT_LIST_TYPE__ALERT_RECEIVER_EMAIL = 0;

  /**
   * The number of structural features of the '<em>Alert List Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALERT_LIST_TYPE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link sitecheck.usatoday.com.sitecheckerschema.impl.AlertReceiverTypeImpl <em>Alert Receiver Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see sitecheck.usatoday.com.sitecheckerschema.impl.AlertReceiverTypeImpl
   * @see sitecheck.usatoday.com.sitecheckerschema.impl.SitecheckerschemaPackageImpl#getAlertReceiverType()
   * @generated
   */
  int ALERT_RECEIVER_TYPE = 1;

  /**
   * The feature id for the '<em><b>Email Address</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALERT_RECEIVER_TYPE__EMAIL_ADDRESS = 0;

  /**
   * The number of structural features of the '<em>Alert Receiver Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ALERT_RECEIVER_TYPE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link sitecheck.usatoday.com.sitecheckerschema.impl.DocumentRootImpl <em>Document Root</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see sitecheck.usatoday.com.sitecheckerschema.impl.DocumentRootImpl
   * @see sitecheck.usatoday.com.sitecheckerschema.impl.SitecheckerschemaPackageImpl#getDocumentRoot()
   * @generated
   */
  int DOCUMENT_ROOT = 2;

  /**
   * The feature id for the '<em><b>Mixed</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__MIXED = 0;

  /**
   * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

  /**
   * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

  /**
   * The feature id for the '<em><b>Site Checker</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT__SITE_CHECKER = 3;

  /**
   * The number of structural features of the '<em>Document Root</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int DOCUMENT_ROOT_FEATURE_COUNT = 4;

  /**
   * The meta object id for the '{@link sitecheck.usatoday.com.sitecheckerschema.impl.ErrorPageListImpl <em>Error Page List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see sitecheck.usatoday.com.sitecheckerschema.impl.ErrorPageListImpl
   * @see sitecheck.usatoday.com.sitecheckerschema.impl.SitecheckerschemaPackageImpl#getErrorPageList()
   * @generated
   */
  int ERROR_PAGE_LIST = 3;

  /**
   * The feature id for the '<em><b>Error Page</b></em>' attribute list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERROR_PAGE_LIST__ERROR_PAGE = 0;

  /**
   * The number of structural features of the '<em>Error Page List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int ERROR_PAGE_LIST_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link sitecheck.usatoday.com.sitecheckerschema.impl.SiteCheckerTypeImpl <em>Site Checker Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see sitecheck.usatoday.com.sitecheckerschema.impl.SiteCheckerTypeImpl
   * @see sitecheck.usatoday.com.sitecheckerschema.impl.SitecheckerschemaPackageImpl#getSiteCheckerType()
   * @generated
   */
  int SITE_CHECKER_TYPE = 4;

  /**
   * The feature id for the '<em><b>Smtp Provider</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SITE_CHECKER_TYPE__SMTP_PROVIDER = 0;

  /**
   * The feature id for the '<em><b>Site</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SITE_CHECKER_TYPE__SITE = 1;

  /**
   * The number of structural features of the '<em>Site Checker Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SITE_CHECKER_TYPE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link sitecheck.usatoday.com.sitecheckerschema.impl.SiteCheckerType1Impl <em>Site Checker Type1</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see sitecheck.usatoday.com.sitecheckerschema.impl.SiteCheckerType1Impl
   * @see sitecheck.usatoday.com.sitecheckerschema.impl.SitecheckerschemaPackageImpl#getSiteCheckerType1()
   * @generated
   */
  int SITE_CHECKER_TYPE1 = 5;

  /**
   * The feature id for the '<em><b>Smtp Provider</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SITE_CHECKER_TYPE1__SMTP_PROVIDER = 0;

  /**
   * The feature id for the '<em><b>Site</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SITE_CHECKER_TYPE1__SITE = 1;

  /**
   * The number of structural features of the '<em>Site Checker Type1</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SITE_CHECKER_TYPE1_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link sitecheck.usatoday.com.sitecheckerschema.impl.SiteTypeImpl <em>Site Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see sitecheck.usatoday.com.sitecheckerschema.impl.SiteTypeImpl
   * @see sitecheck.usatoday.com.sitecheckerschema.impl.SitecheckerschemaPackageImpl#getSiteType()
   * @generated
   */
  int SITE_TYPE = 6;

  /**
   * The feature id for the '<em><b>URL Path</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SITE_TYPE__URL_PATH = 0;

  /**
   * The feature id for the '<em><b>Check Interval</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SITE_TYPE__CHECK_INTERVAL = 1;

  /**
   * The feature id for the '<em><b>Response Time Threshold</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SITE_TYPE__RESPONSE_TIME_THRESHOLD = 2;

  /**
   * The feature id for the '<em><b>Unable To Connect Retry Attempts</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SITE_TYPE__UNABLE_TO_CONNECT_RETRY_ATTEMPTS = 3;

  /**
   * The feature id for the '<em><b>Alert List</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SITE_TYPE__ALERT_LIST = 4;

  /**
   * The feature id for the '<em><b>Alert Interval</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SITE_TYPE__ALERT_INTERVAL = 5;

  /**
   * The feature id for the '<em><b>Log File</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SITE_TYPE__LOG_FILE = 6;

  /**
   * The feature id for the '<em><b>Log Level</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SITE_TYPE__LOG_LEVEL = 7;

  /**
   * The feature id for the '<em><b>Notify When Up</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SITE_TYPE__NOTIFY_WHEN_UP = 8;

  /**
   * The feature id for the '<em><b>Error Pages</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SITE_TYPE__ERROR_PAGES = 9;

  /**
   * The number of structural features of the '<em>Site Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SITE_TYPE_FEATURE_COUNT = 10;

  /**
   * The meta object id for the '{@link sitecheck.usatoday.com.sitecheckerschema.impl.SmtpProviderTypeImpl <em>Smtp Provider Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see sitecheck.usatoday.com.sitecheckerschema.impl.SmtpProviderTypeImpl
   * @see sitecheck.usatoday.com.sitecheckerschema.impl.SitecheckerschemaPackageImpl#getSmtpProviderType()
   * @generated
   */
  int SMTP_PROVIDER_TYPE = 7;

  /**
   * The feature id for the '<em><b>From Address</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SMTP_PROVIDER_TYPE__FROM_ADDRESS = 0;

  /**
   * The feature id for the '<em><b>Host</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SMTP_PROVIDER_TYPE__HOST = 1;

  /**
   * The number of structural features of the '<em>Smtp Provider Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int SMTP_PROVIDER_TYPE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link sitecheck.usatoday.com.sitecheckerschema.impl.URLSequenceImpl <em>URL Sequence</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see sitecheck.usatoday.com.sitecheckerschema.impl.URLSequenceImpl
   * @see sitecheck.usatoday.com.sitecheckerschema.impl.SitecheckerschemaPackageImpl#getURLSequence()
   * @generated
   */
  int URL_SEQUENCE = 8;

  /**
   * The feature id for the '<em><b>URL</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int URL_SEQUENCE__URL = 0;

  /**
   * The number of structural features of the '<em>URL Sequence</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int URL_SEQUENCE_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link sitecheck.usatoday.com.sitecheckerschema.impl.URLTypeImpl <em>URL Type</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see sitecheck.usatoday.com.sitecheckerschema.impl.URLTypeImpl
   * @see sitecheck.usatoday.com.sitecheckerschema.impl.SitecheckerschemaPackageImpl#getURLType()
   * @generated
   */
  int URL_TYPE = 9;

  /**
   * The feature id for the '<em><b>URL</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int URL_TYPE__URL = 0;

  /**
   * The feature id for the '<em><b>Method</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int URL_TYPE__METHOD = 1;

  /**
   * The feature id for the '<em><b>User</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int URL_TYPE__USER = 2;

  /**
   * The feature id for the '<em><b>Pwd</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int URL_TYPE__PWD = 3;

  /**
   * The number of structural features of the '<em>URL Type</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int URL_TYPE_FEATURE_COUNT = 4;


  /**
   * Returns the meta object for class '{@link sitecheck.usatoday.com.sitecheckerschema.AlertListType <em>Alert List Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Alert List Type</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.AlertListType
   * @generated
   */
  EClass getAlertListType();

  /**
   * Returns the meta object for the attribute list '{@link sitecheck.usatoday.com.sitecheckerschema.AlertListType#getAlertReceiverEmail <em>Alert Receiver Email</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Alert Receiver Email</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.AlertListType#getAlertReceiverEmail()
   * @see #getAlertListType()
   * @generated
   */
  EAttribute getAlertListType_AlertReceiverEmail();

  /**
   * Returns the meta object for class '{@link sitecheck.usatoday.com.sitecheckerschema.AlertReceiverType <em>Alert Receiver Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Alert Receiver Type</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.AlertReceiverType
   * @generated
   */
  EClass getAlertReceiverType();

  /**
   * Returns the meta object for the attribute '{@link sitecheck.usatoday.com.sitecheckerschema.AlertReceiverType#getEmailAddress <em>Email Address</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Email Address</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.AlertReceiverType#getEmailAddress()
   * @see #getAlertReceiverType()
   * @generated
   */
  EAttribute getAlertReceiverType_EmailAddress();

  /**
   * Returns the meta object for class '{@link sitecheck.usatoday.com.sitecheckerschema.DocumentRoot <em>Document Root</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Document Root</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.DocumentRoot
   * @generated
   */
  EClass getDocumentRoot();

  /**
   * Returns the meta object for the attribute list '{@link sitecheck.usatoday.com.sitecheckerschema.DocumentRoot#getMixed <em>Mixed</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Mixed</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.DocumentRoot#getMixed()
   * @see #getDocumentRoot()
   * @generated
   */
  EAttribute getDocumentRoot_Mixed();

  /**
   * Returns the meta object for the map '{@link sitecheck.usatoday.com.sitecheckerschema.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.DocumentRoot#getXMLNSPrefixMap()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_XMLNSPrefixMap();

  /**
   * Returns the meta object for the map '{@link sitecheck.usatoday.com.sitecheckerschema.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the map '<em>XSI Schema Location</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.DocumentRoot#getXSISchemaLocation()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_XSISchemaLocation();

  /**
   * Returns the meta object for the containment reference '{@link sitecheck.usatoday.com.sitecheckerschema.DocumentRoot#getSiteChecker <em>Site Checker</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Site Checker</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.DocumentRoot#getSiteChecker()
   * @see #getDocumentRoot()
   * @generated
   */
  EReference getDocumentRoot_SiteChecker();

  /**
   * Returns the meta object for class '{@link sitecheck.usatoday.com.sitecheckerschema.ErrorPageList <em>Error Page List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Error Page List</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.ErrorPageList
   * @generated
   */
  EClass getErrorPageList();

  /**
   * Returns the meta object for the attribute list '{@link sitecheck.usatoday.com.sitecheckerschema.ErrorPageList#getErrorPage <em>Error Page</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute list '<em>Error Page</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.ErrorPageList#getErrorPage()
   * @see #getErrorPageList()
   * @generated
   */
  EAttribute getErrorPageList_ErrorPage();

  /**
   * Returns the meta object for class '{@link sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType <em>Site Checker Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Site Checker Type</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType
   * @generated
   */
  EClass getSiteCheckerType();

  /**
   * Returns the meta object for the containment reference '{@link sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType#getSmtpProvider <em>Smtp Provider</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Smtp Provider</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType#getSmtpProvider()
   * @see #getSiteCheckerType()
   * @generated
   */
  EReference getSiteCheckerType_SmtpProvider();

  /**
   * Returns the meta object for the containment reference list '{@link sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType#getSite <em>Site</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Site</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType#getSite()
   * @see #getSiteCheckerType()
   * @generated
   */
  EReference getSiteCheckerType_Site();

  /**
   * Returns the meta object for class '{@link sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType1 <em>Site Checker Type1</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Site Checker Type1</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType1
   * @generated
   */
  EClass getSiteCheckerType1();

  /**
   * Returns the meta object for the containment reference '{@link sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType1#getSmtpProvider <em>Smtp Provider</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Smtp Provider</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType1#getSmtpProvider()
   * @see #getSiteCheckerType1()
   * @generated
   */
  EReference getSiteCheckerType1_SmtpProvider();

  /**
   * Returns the meta object for the containment reference list '{@link sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType1#getSite <em>Site</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Site</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType1#getSite()
   * @see #getSiteCheckerType1()
   * @generated
   */
  EReference getSiteCheckerType1_Site();

  /**
   * Returns the meta object for class '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType <em>Site Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Site Type</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.SiteType
   * @generated
   */
  EClass getSiteType();

  /**
   * Returns the meta object for the containment reference '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getURLPath <em>URL Path</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>URL Path</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.SiteType#getURLPath()
   * @see #getSiteType()
   * @generated
   */
  EReference getSiteType_URLPath();

  /**
   * Returns the meta object for the attribute '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getCheckInterval <em>Check Interval</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Check Interval</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.SiteType#getCheckInterval()
   * @see #getSiteType()
   * @generated
   */
  EAttribute getSiteType_CheckInterval();

  /**
   * Returns the meta object for the attribute '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getResponseTimeThreshold <em>Response Time Threshold</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Response Time Threshold</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.SiteType#getResponseTimeThreshold()
   * @see #getSiteType()
   * @generated
   */
  EAttribute getSiteType_ResponseTimeThreshold();

  /**
   * Returns the meta object for the attribute '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getUnableToConnectRetryAttempts <em>Unable To Connect Retry Attempts</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Unable To Connect Retry Attempts</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.SiteType#getUnableToConnectRetryAttempts()
   * @see #getSiteType()
   * @generated
   */
  EAttribute getSiteType_UnableToConnectRetryAttempts();

  /**
   * Returns the meta object for the containment reference '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getAlertList <em>Alert List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Alert List</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.SiteType#getAlertList()
   * @see #getSiteType()
   * @generated
   */
  EReference getSiteType_AlertList();

  /**
   * Returns the meta object for the attribute '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getAlertInterval <em>Alert Interval</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Alert Interval</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.SiteType#getAlertInterval()
   * @see #getSiteType()
   * @generated
   */
  EAttribute getSiteType_AlertInterval();

  /**
   * Returns the meta object for the attribute '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getLogFile <em>Log File</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Log File</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.SiteType#getLogFile()
   * @see #getSiteType()
   * @generated
   */
  EAttribute getSiteType_LogFile();

  /**
   * Returns the meta object for the attribute '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getLogLevel <em>Log Level</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Log Level</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.SiteType#getLogLevel()
   * @see #getSiteType()
   * @generated
   */
  EAttribute getSiteType_LogLevel();

  /**
   * Returns the meta object for the attribute '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#isNotifyWhenUp <em>Notify When Up</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Notify When Up</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.SiteType#isNotifyWhenUp()
   * @see #getSiteType()
   * @generated
   */
  EAttribute getSiteType_NotifyWhenUp();

  /**
   * Returns the meta object for the containment reference '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType#getErrorPages <em>Error Pages</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Error Pages</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.SiteType#getErrorPages()
   * @see #getSiteType()
   * @generated
   */
  EReference getSiteType_ErrorPages();

  /**
   * Returns the meta object for class '{@link sitecheck.usatoday.com.sitecheckerschema.SmtpProviderType <em>Smtp Provider Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Smtp Provider Type</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.SmtpProviderType
   * @generated
   */
  EClass getSmtpProviderType();

  /**
   * Returns the meta object for the attribute '{@link sitecheck.usatoday.com.sitecheckerschema.SmtpProviderType#getFromAddress <em>From Address</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>From Address</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.SmtpProviderType#getFromAddress()
   * @see #getSmtpProviderType()
   * @generated
   */
  EAttribute getSmtpProviderType_FromAddress();

  /**
   * Returns the meta object for the attribute '{@link sitecheck.usatoday.com.sitecheckerschema.SmtpProviderType#getHost <em>Host</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Host</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.SmtpProviderType#getHost()
   * @see #getSmtpProviderType()
   * @generated
   */
  EAttribute getSmtpProviderType_Host();

  /**
   * Returns the meta object for class '{@link sitecheck.usatoday.com.sitecheckerschema.URLSequence <em>URL Sequence</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>URL Sequence</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.URLSequence
   * @generated
   */
  EClass getURLSequence();

  /**
   * Returns the meta object for the containment reference list '{@link sitecheck.usatoday.com.sitecheckerschema.URLSequence#getURL <em>URL</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>URL</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.URLSequence#getURL()
   * @see #getURLSequence()
   * @generated
   */
  EReference getURLSequence_URL();

  /**
   * Returns the meta object for class '{@link sitecheck.usatoday.com.sitecheckerschema.URLType <em>URL Type</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>URL Type</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.URLType
   * @generated
   */
  EClass getURLType();

  /**
   * Returns the meta object for the attribute '{@link sitecheck.usatoday.com.sitecheckerschema.URLType#getURL <em>URL</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>URL</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.URLType#getURL()
   * @see #getURLType()
   * @generated
   */
  EAttribute getURLType_URL();

  /**
   * Returns the meta object for the attribute '{@link sitecheck.usatoday.com.sitecheckerschema.URLType#getMethod <em>Method</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Method</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.URLType#getMethod()
   * @see #getURLType()
   * @generated
   */
  EAttribute getURLType_Method();

  /**
   * Returns the meta object for the attribute '{@link sitecheck.usatoday.com.sitecheckerschema.URLType#getUser <em>User</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>User</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.URLType#getUser()
   * @see #getURLType()
   * @generated
   */
  EAttribute getURLType_User();

  /**
   * Returns the meta object for the attribute '{@link sitecheck.usatoday.com.sitecheckerschema.URLType#getPwd <em>Pwd</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Pwd</em>'.
   * @see sitecheck.usatoday.com.sitecheckerschema.URLType#getPwd()
   * @see #getURLType()
   * @generated
   */
  EAttribute getURLType_Pwd();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  SitecheckerschemaFactory getSitecheckerschemaFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link sitecheck.usatoday.com.sitecheckerschema.impl.AlertListTypeImpl <em>Alert List Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see sitecheck.usatoday.com.sitecheckerschema.impl.AlertListTypeImpl
     * @see sitecheck.usatoday.com.sitecheckerschema.impl.SitecheckerschemaPackageImpl#getAlertListType()
     * @generated
     */
    EClass ALERT_LIST_TYPE = eINSTANCE.getAlertListType();

    /**
     * The meta object literal for the '<em><b>Alert Receiver Email</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ALERT_LIST_TYPE__ALERT_RECEIVER_EMAIL = eINSTANCE.getAlertListType_AlertReceiverEmail();

    /**
     * The meta object literal for the '{@link sitecheck.usatoday.com.sitecheckerschema.impl.AlertReceiverTypeImpl <em>Alert Receiver Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see sitecheck.usatoday.com.sitecheckerschema.impl.AlertReceiverTypeImpl
     * @see sitecheck.usatoday.com.sitecheckerschema.impl.SitecheckerschemaPackageImpl#getAlertReceiverType()
     * @generated
     */
    EClass ALERT_RECEIVER_TYPE = eINSTANCE.getAlertReceiverType();

    /**
     * The meta object literal for the '<em><b>Email Address</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ALERT_RECEIVER_TYPE__EMAIL_ADDRESS = eINSTANCE.getAlertReceiverType_EmailAddress();

    /**
     * The meta object literal for the '{@link sitecheck.usatoday.com.sitecheckerschema.impl.DocumentRootImpl <em>Document Root</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see sitecheck.usatoday.com.sitecheckerschema.impl.DocumentRootImpl
     * @see sitecheck.usatoday.com.sitecheckerschema.impl.SitecheckerschemaPackageImpl#getDocumentRoot()
     * @generated
     */
    EClass DOCUMENT_ROOT = eINSTANCE.getDocumentRoot();

    /**
     * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute DOCUMENT_ROOT__MIXED = eINSTANCE.getDocumentRoot_Mixed();

    /**
     * The meta object literal for the '<em><b>XMLNS Prefix Map</b></em>' map feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT_ROOT__XMLNS_PREFIX_MAP = eINSTANCE.getDocumentRoot_XMLNSPrefixMap();

    /**
     * The meta object literal for the '<em><b>XSI Schema Location</b></em>' map feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = eINSTANCE.getDocumentRoot_XSISchemaLocation();

    /**
     * The meta object literal for the '<em><b>Site Checker</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference DOCUMENT_ROOT__SITE_CHECKER = eINSTANCE.getDocumentRoot_SiteChecker();

    /**
     * The meta object literal for the '{@link sitecheck.usatoday.com.sitecheckerschema.impl.ErrorPageListImpl <em>Error Page List</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see sitecheck.usatoday.com.sitecheckerschema.impl.ErrorPageListImpl
     * @see sitecheck.usatoday.com.sitecheckerschema.impl.SitecheckerschemaPackageImpl#getErrorPageList()
     * @generated
     */
    EClass ERROR_PAGE_LIST = eINSTANCE.getErrorPageList();

    /**
     * The meta object literal for the '<em><b>Error Page</b></em>' attribute list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute ERROR_PAGE_LIST__ERROR_PAGE = eINSTANCE.getErrorPageList_ErrorPage();

    /**
     * The meta object literal for the '{@link sitecheck.usatoday.com.sitecheckerschema.impl.SiteCheckerTypeImpl <em>Site Checker Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see sitecheck.usatoday.com.sitecheckerschema.impl.SiteCheckerTypeImpl
     * @see sitecheck.usatoday.com.sitecheckerschema.impl.SitecheckerschemaPackageImpl#getSiteCheckerType()
     * @generated
     */
    EClass SITE_CHECKER_TYPE = eINSTANCE.getSiteCheckerType();

    /**
     * The meta object literal for the '<em><b>Smtp Provider</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SITE_CHECKER_TYPE__SMTP_PROVIDER = eINSTANCE.getSiteCheckerType_SmtpProvider();

    /**
     * The meta object literal for the '<em><b>Site</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SITE_CHECKER_TYPE__SITE = eINSTANCE.getSiteCheckerType_Site();

    /**
     * The meta object literal for the '{@link sitecheck.usatoday.com.sitecheckerschema.impl.SiteCheckerType1Impl <em>Site Checker Type1</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see sitecheck.usatoday.com.sitecheckerschema.impl.SiteCheckerType1Impl
     * @see sitecheck.usatoday.com.sitecheckerschema.impl.SitecheckerschemaPackageImpl#getSiteCheckerType1()
     * @generated
     */
    EClass SITE_CHECKER_TYPE1 = eINSTANCE.getSiteCheckerType1();

    /**
     * The meta object literal for the '<em><b>Smtp Provider</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SITE_CHECKER_TYPE1__SMTP_PROVIDER = eINSTANCE.getSiteCheckerType1_SmtpProvider();

    /**
     * The meta object literal for the '<em><b>Site</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SITE_CHECKER_TYPE1__SITE = eINSTANCE.getSiteCheckerType1_Site();

    /**
     * The meta object literal for the '{@link sitecheck.usatoday.com.sitecheckerschema.impl.SiteTypeImpl <em>Site Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see sitecheck.usatoday.com.sitecheckerschema.impl.SiteTypeImpl
     * @see sitecheck.usatoday.com.sitecheckerschema.impl.SitecheckerschemaPackageImpl#getSiteType()
     * @generated
     */
    EClass SITE_TYPE = eINSTANCE.getSiteType();

    /**
     * The meta object literal for the '<em><b>URL Path</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SITE_TYPE__URL_PATH = eINSTANCE.getSiteType_URLPath();

    /**
     * The meta object literal for the '<em><b>Check Interval</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SITE_TYPE__CHECK_INTERVAL = eINSTANCE.getSiteType_CheckInterval();

    /**
     * The meta object literal for the '<em><b>Response Time Threshold</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SITE_TYPE__RESPONSE_TIME_THRESHOLD = eINSTANCE.getSiteType_ResponseTimeThreshold();

    /**
     * The meta object literal for the '<em><b>Unable To Connect Retry Attempts</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SITE_TYPE__UNABLE_TO_CONNECT_RETRY_ATTEMPTS = eINSTANCE.getSiteType_UnableToConnectRetryAttempts();

    /**
     * The meta object literal for the '<em><b>Alert List</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SITE_TYPE__ALERT_LIST = eINSTANCE.getSiteType_AlertList();

    /**
     * The meta object literal for the '<em><b>Alert Interval</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SITE_TYPE__ALERT_INTERVAL = eINSTANCE.getSiteType_AlertInterval();

    /**
     * The meta object literal for the '<em><b>Log File</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SITE_TYPE__LOG_FILE = eINSTANCE.getSiteType_LogFile();

    /**
     * The meta object literal for the '<em><b>Log Level</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SITE_TYPE__LOG_LEVEL = eINSTANCE.getSiteType_LogLevel();

    /**
     * The meta object literal for the '<em><b>Notify When Up</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SITE_TYPE__NOTIFY_WHEN_UP = eINSTANCE.getSiteType_NotifyWhenUp();

    /**
     * The meta object literal for the '<em><b>Error Pages</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference SITE_TYPE__ERROR_PAGES = eINSTANCE.getSiteType_ErrorPages();

    /**
     * The meta object literal for the '{@link sitecheck.usatoday.com.sitecheckerschema.impl.SmtpProviderTypeImpl <em>Smtp Provider Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see sitecheck.usatoday.com.sitecheckerschema.impl.SmtpProviderTypeImpl
     * @see sitecheck.usatoday.com.sitecheckerschema.impl.SitecheckerschemaPackageImpl#getSmtpProviderType()
     * @generated
     */
    EClass SMTP_PROVIDER_TYPE = eINSTANCE.getSmtpProviderType();

    /**
     * The meta object literal for the '<em><b>From Address</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SMTP_PROVIDER_TYPE__FROM_ADDRESS = eINSTANCE.getSmtpProviderType_FromAddress();

    /**
     * The meta object literal for the '<em><b>Host</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute SMTP_PROVIDER_TYPE__HOST = eINSTANCE.getSmtpProviderType_Host();

    /**
     * The meta object literal for the '{@link sitecheck.usatoday.com.sitecheckerschema.impl.URLSequenceImpl <em>URL Sequence</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see sitecheck.usatoday.com.sitecheckerschema.impl.URLSequenceImpl
     * @see sitecheck.usatoday.com.sitecheckerschema.impl.SitecheckerschemaPackageImpl#getURLSequence()
     * @generated
     */
    EClass URL_SEQUENCE = eINSTANCE.getURLSequence();

    /**
     * The meta object literal for the '<em><b>URL</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference URL_SEQUENCE__URL = eINSTANCE.getURLSequence_URL();

    /**
     * The meta object literal for the '{@link sitecheck.usatoday.com.sitecheckerschema.impl.URLTypeImpl <em>URL Type</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see sitecheck.usatoday.com.sitecheckerschema.impl.URLTypeImpl
     * @see sitecheck.usatoday.com.sitecheckerschema.impl.SitecheckerschemaPackageImpl#getURLType()
     * @generated
     */
    EClass URL_TYPE = eINSTANCE.getURLType();

    /**
     * The meta object literal for the '<em><b>URL</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute URL_TYPE__URL = eINSTANCE.getURLType_URL();

    /**
     * The meta object literal for the '<em><b>Method</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute URL_TYPE__METHOD = eINSTANCE.getURLType_Method();

    /**
     * The meta object literal for the '<em><b>User</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute URL_TYPE__USER = eINSTANCE.getURLType_User();

    /**
     * The meta object literal for the '<em><b>Pwd</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute URL_TYPE__PWD = eINSTANCE.getURLType_Pwd();

  }

} //SitecheckerschemaPackage
