/**
 * <copyright>
 * </copyright>
 *
 * $Id: SiteCheckerType.java,v 1.1 2011/01/17 20:25:51 aeast Exp $
 */
package sitecheck.usatoday.com.sitecheckerschema;

import java.util.List;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Site Checker Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType#getSmtpProvider <em>Smtp Provider</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType#getSite <em>Site</em>}</li>
 * </ul>
 * </p>
 *
 * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getSiteCheckerType()
 * @model extendedMetaData="name='SiteCheckerType' kind='elementOnly'"
 * @generated
 */
public interface SiteCheckerType
{
  /**
   * Returns the value of the '<em><b>Smtp Provider</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Smtp Provider</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Smtp Provider</em>' containment reference.
   * @see #setSmtpProvider(SmtpProviderType)
   * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getSiteCheckerType_SmtpProvider()
   * @model containment="true"
   *        extendedMetaData="kind='element' name='SmtpProvider'"
   * @generated
   */
  SmtpProviderType getSmtpProvider();

  /**
   * Sets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType#getSmtpProvider <em>Smtp Provider</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Smtp Provider</em>' containment reference.
   * @see #getSmtpProvider()
   * @generated
   */
  void setSmtpProvider(SmtpProviderType value);

  /**
   * Returns the value of the '<em><b>Site</b></em>' containment reference list.
   * The list contents are of type {@link sitecheck.usatoday.com.sitecheckerschema.SiteType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Site</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Site</em>' containment reference list.
   * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getSiteCheckerType_Site()
   * @model containment="true" upper="10"
   *        extendedMetaData="kind='element' name='Site'"
   * @generated
   */
  List<SiteType> getSite();

} // SiteCheckerType
