/**
 * <copyright>
 * </copyright>
 *
 * $Id: ErrorPageList.java,v 1.1 2011/01/17 20:25:51 aeast Exp $
 */
package sitecheck.usatoday.com.sitecheckerschema;

import java.util.List;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Error Page List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * 
 * 				This should be the relative URL of the error page.
 * 				Exampe: /err/err.html
 * 			
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.ErrorPageList#getErrorPage <em>Error Page</em>}</li>
 * </ul>
 * </p>
 *
 * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getErrorPageList()
 * @model extendedMetaData="name='ErrorPageList' kind='elementOnly'"
 * @generated
 */
public interface ErrorPageList
{
  /**
   * Returns the value of the '<em><b>Error Page</b></em>' attribute list.
   * The list contents are of type {@link java.lang.String}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Error Page</em>' attribute list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Error Page</em>' attribute list.
   * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getErrorPageList_ErrorPage()
   * @model unique="false" dataType="org.eclipse.emf.ecore.xml.type.String"
   *        extendedMetaData="kind='element' name='ErrorPage'"
   * @generated
   */
  List<String> getErrorPage();

} // ErrorPageList
