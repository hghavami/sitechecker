/**
 * <copyright>
 * </copyright>
 *
 * $Id: SiteCheckerType1.java,v 1.1 2011/01/17 20:25:51 aeast Exp $
 */
package sitecheck.usatoday.com.sitecheckerschema;

import java.util.List;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Site Checker Type1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType1#getSmtpProvider <em>Smtp Provider</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType1#getSite <em>Site</em>}</li>
 * </ul>
 * </p>
 *
 * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getSiteCheckerType1()
 * @model extendedMetaData="name='SiteChecker_._type' kind='elementOnly'"
 * @generated
 */
public interface SiteCheckerType1
{
  /**
   * Returns the value of the '<em><b>Smtp Provider</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Smtp Provider</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Smtp Provider</em>' containment reference.
   * @see #setSmtpProvider(SmtpProviderType)
   * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getSiteCheckerType1_SmtpProvider()
   * @model containment="true" required="true"
   *        extendedMetaData="kind='element' name='SmtpProvider'"
   * @generated
   */
  SmtpProviderType getSmtpProvider();

  /**
   * Sets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType1#getSmtpProvider <em>Smtp Provider</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Smtp Provider</em>' containment reference.
   * @see #getSmtpProvider()
   * @generated
   */
  void setSmtpProvider(SmtpProviderType value);

  /**
   * Returns the value of the '<em><b>Site</b></em>' containment reference list.
   * The list contents are of type {@link sitecheck.usatoday.com.sitecheckerschema.SiteType}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Site</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Site</em>' containment reference list.
   * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getSiteCheckerType1_Site()
   * @model containment="true" required="true"
   *        extendedMetaData="kind='element' name='Site'"
   * @generated
   */
  List<SiteType> getSite();

} // SiteCheckerType1
