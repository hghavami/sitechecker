/**
 * <copyright>
 * </copyright>
 *
 * $Id: SitecheckerschemaAdapterFactory.java,v 1.1 2011/01/17 20:25:51 aeast Exp $
 */
package sitecheck.usatoday.com.sitecheckerschema.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import sitecheck.usatoday.com.sitecheckerschema.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage
 * @generated
 */
public class SitecheckerschemaAdapterFactory extends AdapterFactoryImpl
{
  /**
   * The cached model package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static SitecheckerschemaPackage modelPackage;

  /**
   * Creates an instance of the adapter factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SitecheckerschemaAdapterFactory()
  {
    if (modelPackage == null)
    {
      modelPackage = SitecheckerschemaPackage.eINSTANCE;
    }
  }

  /**
   * Returns whether this factory is applicable for the type of the object.
   * <!-- begin-user-doc -->
   * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
   * <!-- end-user-doc -->
   * @return whether this factory is applicable for the type of the object.
   * @generated
   */
  @Override
  public boolean isFactoryForType(Object object)
  {
    if (object == modelPackage)
    {
      return true;
    }
    if (object instanceof EObject)
    {
      return ((EObject)object).eClass().getEPackage() == modelPackage;
    }
    return false;
  }

  /**
   * The switch that delegates to the <code>createXXX</code> methods.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SitecheckerschemaSwitch<Adapter> modelSwitch =
    new SitecheckerschemaSwitch<Adapter>()
    {
      @Override
      public Adapter caseAlertListType(AlertListType object)
      {
        return createAlertListTypeAdapter();
      }
      @Override
      public Adapter caseAlertReceiverType(AlertReceiverType object)
      {
        return createAlertReceiverTypeAdapter();
      }
      @Override
      public Adapter caseDocumentRoot(DocumentRoot object)
      {
        return createDocumentRootAdapter();
      }
      @Override
      public Adapter caseErrorPageList(ErrorPageList object)
      {
        return createErrorPageListAdapter();
      }
      @Override
      public Adapter caseSiteCheckerType(SiteCheckerType object)
      {
        return createSiteCheckerTypeAdapter();
      }
      @Override
      public Adapter caseSiteCheckerType1(SiteCheckerType1 object)
      {
        return createSiteCheckerType1Adapter();
      }
      @Override
      public Adapter caseSiteType(SiteType object)
      {
        return createSiteTypeAdapter();
      }
      @Override
      public Adapter caseSmtpProviderType(SmtpProviderType object)
      {
        return createSmtpProviderTypeAdapter();
      }
      @Override
      public Adapter caseURLSequence(URLSequence object)
      {
        return createURLSequenceAdapter();
      }
      @Override
      public Adapter caseURLType(URLType object)
      {
        return createURLTypeAdapter();
      }
      @Override
      public Adapter defaultCase(EObject object)
      {
        return createEObjectAdapter();
      }
    };

  /**
   * Creates an adapter for the <code>target</code>.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param target the object to adapt.
   * @return the adapter for the <code>target</code>.
   * @generated
   */
  @Override
  public Adapter createAdapter(Notifier target)
  {
    return modelSwitch.doSwitch((EObject)target);
  }


  /**
   * Creates a new adapter for an object of class '{@link sitecheck.usatoday.com.sitecheckerschema.AlertListType <em>Alert List Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see sitecheck.usatoday.com.sitecheckerschema.AlertListType
   * @generated
   */
  public Adapter createAlertListTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link sitecheck.usatoday.com.sitecheckerschema.AlertReceiverType <em>Alert Receiver Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see sitecheck.usatoday.com.sitecheckerschema.AlertReceiverType
   * @generated
   */
  public Adapter createAlertReceiverTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link sitecheck.usatoday.com.sitecheckerschema.DocumentRoot <em>Document Root</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see sitecheck.usatoday.com.sitecheckerschema.DocumentRoot
   * @generated
   */
  public Adapter createDocumentRootAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link sitecheck.usatoday.com.sitecheckerschema.ErrorPageList <em>Error Page List</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see sitecheck.usatoday.com.sitecheckerschema.ErrorPageList
   * @generated
   */
  public Adapter createErrorPageListAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType <em>Site Checker Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType
   * @generated
   */
  public Adapter createSiteCheckerTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType1 <em>Site Checker Type1</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType1
   * @generated
   */
  public Adapter createSiteCheckerType1Adapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link sitecheck.usatoday.com.sitecheckerschema.SiteType <em>Site Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see sitecheck.usatoday.com.sitecheckerschema.SiteType
   * @generated
   */
  public Adapter createSiteTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link sitecheck.usatoday.com.sitecheckerschema.SmtpProviderType <em>Smtp Provider Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see sitecheck.usatoday.com.sitecheckerschema.SmtpProviderType
   * @generated
   */
  public Adapter createSmtpProviderTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link sitecheck.usatoday.com.sitecheckerschema.URLSequence <em>URL Sequence</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see sitecheck.usatoday.com.sitecheckerschema.URLSequence
   * @generated
   */
  public Adapter createURLSequenceAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for an object of class '{@link sitecheck.usatoday.com.sitecheckerschema.URLType <em>URL Type</em>}'.
   * <!-- begin-user-doc -->
   * This default implementation returns null so that we can easily ignore cases;
   * it's useful to ignore a case when inheritance will catch all the cases anyway.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @see sitecheck.usatoday.com.sitecheckerschema.URLType
   * @generated
   */
  public Adapter createURLTypeAdapter()
  {
    return null;
  }

  /**
   * Creates a new adapter for the default case.
   * <!-- begin-user-doc -->
   * This default implementation returns null.
   * <!-- end-user-doc -->
   * @return the new adapter.
   * @generated
   */
  public Adapter createEObjectAdapter()
  {
    return null;
  }

} //SitecheckerschemaAdapterFactory
