/**
 * <copyright>
 * </copyright>
 *
 * $Id: SitecheckerschemaResourceImpl.java,v 1.1 2011/01/17 20:25:51 aeast Exp $
 */
package sitecheck.usatoday.com.sitecheckerschema.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see sitecheck.usatoday.com.sitecheckerschema.util.SitecheckerschemaResourceFactoryImpl
 * @generated
 */
public class SitecheckerschemaResourceImpl extends XMLResourceImpl
{
  /**
   * Creates an instance of the resource.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param uri the URI of the new resource.
   * @generated
   */
  public SitecheckerschemaResourceImpl(URI uri)
  {
    super(uri);
  }

} //SitecheckerschemaResourceFactoryImpl
