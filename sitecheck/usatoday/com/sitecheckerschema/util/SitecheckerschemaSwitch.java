/**
 * <copyright>
 * </copyright>
 *
 * $Id: SitecheckerschemaSwitch.java,v 1.1 2011/01/17 20:25:51 aeast Exp $
 */
package sitecheck.usatoday.com.sitecheckerschema.util;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import sitecheck.usatoday.com.sitecheckerschema.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage
 * @generated
 */
public class SitecheckerschemaSwitch<T>
{
  /**
   * The cached model package
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected static SitecheckerschemaPackage modelPackage;

  /**
   * Creates an instance of the switch.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SitecheckerschemaSwitch()
  {
    if (modelPackage == null)
    {
      modelPackage = SitecheckerschemaPackage.eINSTANCE;
    }
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  public T doSwitch(EObject theEObject)
  {
    return doSwitch(theEObject.eClass(), theEObject);
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  @SuppressWarnings("unchecked")
protected T doSwitch(EClass theEClass, EObject theEObject)
  {
    if (theEClass.eContainer() == modelPackage)
    {
      return doSwitch(theEClass.getClassifierID(), theEObject);
    }
    else
    {
      List<EClass> eSuperTypes = theEClass.getESuperTypes();
      return
        eSuperTypes.isEmpty() ?
          defaultCase(theEObject) :
          doSwitch(eSuperTypes.get(0), theEObject);
    }
  }

  /**
   * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the first non-null result returned by a <code>caseXXX</code> call.
   * @generated
   */
  protected T doSwitch(int classifierID, EObject theEObject)
  {
    switch (classifierID)
    {
      case SitecheckerschemaPackage.ALERT_LIST_TYPE:
      {
        AlertListType alertListType = (AlertListType)theEObject;
        T result = caseAlertListType(alertListType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SitecheckerschemaPackage.ALERT_RECEIVER_TYPE:
      {
        AlertReceiverType alertReceiverType = (AlertReceiverType)theEObject;
        T result = caseAlertReceiverType(alertReceiverType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SitecheckerschemaPackage.DOCUMENT_ROOT:
      {
        DocumentRoot documentRoot = (DocumentRoot)theEObject;
        T result = caseDocumentRoot(documentRoot);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SitecheckerschemaPackage.ERROR_PAGE_LIST:
      {
        ErrorPageList errorPageList = (ErrorPageList)theEObject;
        T result = caseErrorPageList(errorPageList);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SitecheckerschemaPackage.SITE_CHECKER_TYPE:
      {
        SiteCheckerType siteCheckerType = (SiteCheckerType)theEObject;
        T result = caseSiteCheckerType(siteCheckerType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SitecheckerschemaPackage.SITE_CHECKER_TYPE1:
      {
        SiteCheckerType1 siteCheckerType1 = (SiteCheckerType1)theEObject;
        T result = caseSiteCheckerType1(siteCheckerType1);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SitecheckerschemaPackage.SITE_TYPE:
      {
        SiteType siteType = (SiteType)theEObject;
        T result = caseSiteType(siteType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SitecheckerschemaPackage.SMTP_PROVIDER_TYPE:
      {
        SmtpProviderType smtpProviderType = (SmtpProviderType)theEObject;
        T result = caseSmtpProviderType(smtpProviderType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SitecheckerschemaPackage.URL_SEQUENCE:
      {
        URLSequence urlSequence = (URLSequence)theEObject;
        T result = caseURLSequence(urlSequence);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      case SitecheckerschemaPackage.URL_TYPE:
      {
        URLType urlType = (URLType)theEObject;
        T result = caseURLType(urlType);
        if (result == null) result = defaultCase(theEObject);
        return result;
      }
      default: return defaultCase(theEObject);
    }
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Alert List Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Alert List Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAlertListType(AlertListType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Alert Receiver Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Alert Receiver Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseAlertReceiverType(AlertReceiverType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Document Root</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Document Root</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseDocumentRoot(DocumentRoot object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Error Page List</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Error Page List</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseErrorPageList(ErrorPageList object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Site Checker Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Site Checker Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSiteCheckerType(SiteCheckerType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Site Checker Type1</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Site Checker Type1</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSiteCheckerType1(SiteCheckerType1 object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Site Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Site Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSiteType(SiteType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>Smtp Provider Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>Smtp Provider Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseSmtpProviderType(SmtpProviderType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>URL Sequence</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>URL Sequence</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseURLSequence(URLSequence object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>URL Type</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>URL Type</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
   * @generated
   */
  public T caseURLType(URLType object)
  {
    return null;
  }

  /**
   * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
   * <!-- begin-user-doc -->
   * This implementation returns null;
   * returning a non-null result will terminate the switch, but this is the last case anyway.
   * <!-- end-user-doc -->
   * @param object the target of the switch.
   * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
   * @see #doSwitch(org.eclipse.emf.ecore.EObject)
   * @generated
   */
  public T defaultCase(EObject object)
  {
    return null;
  }

} //SitecheckerschemaSwitch
