/**
 * <copyright>
 * </copyright>
 *
 * $Id: SitecheckerschemaXMLProcessor.java,v 1.1 2011/01/17 20:25:51 aeast Exp $
 */
package sitecheck.usatoday.com.sitecheckerschema.util;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.util.XMLProcessor;

import sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SitecheckerschemaXMLProcessor extends XMLProcessor
{

  /**
   * Public constructor to instantiate the helper.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public SitecheckerschemaXMLProcessor()
  {
    super((EPackage.Registry.INSTANCE));
    SitecheckerschemaPackage.eINSTANCE.eClass();
  }
  
  /**
   * Register for "*" and "xml" file extensions the SitecheckerschemaResourceFactoryImpl factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected Map<String, Resource.Factory> getRegistrations()
  {
    if (registrations == null)
    {
      super.getRegistrations();
      registrations.put(XML_EXTENSION, new SitecheckerschemaResourceFactoryImpl());
      registrations.put(STAR_EXTENSION, new SitecheckerschemaResourceFactoryImpl());
    }
    return registrations;
  }

} //SitecheckerschemaXMLProcessor
