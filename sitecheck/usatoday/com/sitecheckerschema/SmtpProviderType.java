/**
 * <copyright>
 * </copyright>
 *
 * $Id: SmtpProviderType.java,v 1.1 2011/01/17 20:25:51 aeast Exp $
 */
package sitecheck.usatoday.com.sitecheckerschema;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Smtp Provider Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.SmtpProviderType#getFromAddress <em>From Address</em>}</li>
 *   <li>{@link sitecheck.usatoday.com.sitecheckerschema.SmtpProviderType#getHost <em>Host</em>}</li>
 * </ul>
 * </p>
 *
 * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getSmtpProviderType()
 * @model extendedMetaData="name='SmtpProviderType' kind='empty'"
 * @generated
 */
public interface SmtpProviderType
{
  /**
   * Returns the value of the '<em><b>From Address</b></em>' attribute.
   * The default value is <code>"aeast@usatoday.com"</code>.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>From Address</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>From Address</em>' attribute.
   * @see #isSetFromAddress()
   * @see #unsetFromAddress()
   * @see #setFromAddress(String)
   * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getSmtpProviderType_FromAddress()
   * @model default="aeast@usatoday.com" unsettable="true" dataType="org.eclipse.emf.ecore.xml.type.String"
   *        extendedMetaData="kind='attribute' name='fromAddress'"
   * @generated
   */
  String getFromAddress();

  /**
   * Sets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SmtpProviderType#getFromAddress <em>From Address</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>From Address</em>' attribute.
   * @see #isSetFromAddress()
   * @see #unsetFromAddress()
   * @see #getFromAddress()
   * @generated
   */
  void setFromAddress(String value);

  /**
   * Unsets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SmtpProviderType#getFromAddress <em>From Address</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #isSetFromAddress()
   * @see #getFromAddress()
   * @see #setFromAddress(String)
   * @generated
   */
  void unsetFromAddress();

  /**
   * Returns whether the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SmtpProviderType#getFromAddress <em>From Address</em>}' attribute is set.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return whether the value of the '<em>From Address</em>' attribute is set.
   * @see #unsetFromAddress()
   * @see #getFromAddress()
   * @see #setFromAddress(String)
   * @generated
   */
  boolean isSetFromAddress();

  /**
   * Returns the value of the '<em><b>Host</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Host</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Host</em>' attribute.
   * @see #setHost(String)
   * @see sitecheck.usatoday.com.sitecheckerschema.SitecheckerschemaPackage#getSmtpProviderType_Host()
   * @model dataType="org.eclipse.emf.ecore.xml.type.String" required="true"
   *        extendedMetaData="kind='attribute' name='host'"
   * @generated
   */
  String getHost();

  /**
   * Sets the value of the '{@link sitecheck.usatoday.com.sitecheckerschema.SmtpProviderType#getHost <em>Host</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Host</em>' attribute.
   * @see #getHost()
   * @generated
   */
  void setHost(String value);

} // SmtpProviderType
