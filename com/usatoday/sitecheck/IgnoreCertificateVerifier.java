package com.usatoday.sitecheck;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;

/**
 * @author Andy East
 * @see javax.net.ssl.HostnameVerifier
 * 
 * The <code>IgnoreCertificateVerifier</code> class is used to ignore invalid digital
 * certificates. This class should not be used except to determin if a HTTPS site is available.
 */
public class IgnoreCertificateVerifier implements HostnameVerifier {

	/**
	 * Constructor for IgnoreCertificateVerifier.
	 */
	public IgnoreCertificateVerifier() {
		super();
	}

	/**
	 * @see com.sun.net.ssl.HostnameVerifier#verify(String, String)
	 */
	public boolean verify(String arg0, SSLSession arg1) {
		return true;
	}

}
