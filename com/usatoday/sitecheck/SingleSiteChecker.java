package com.usatoday.sitecheck;

import java.io.IOException;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

import sitecheck.usatoday.com.sitecheckerschema.AlertListType;
import sitecheck.usatoday.com.sitecheckerschema.ErrorPageList;
import sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType;
import sitecheck.usatoday.com.sitecheckerschema.SiteType;
import sitecheck.usatoday.com.sitecheckerschema.SmtpProviderType;
import sitecheck.usatoday.com.sitecheckerschema.URLSequence;
import sitecheck.usatoday.com.sitecheckerschema.URLType;
import sitecheck.usatoday.com.sitecheckerschema.impl.SitecheckerschemaFactoryImpl;

/**
 * @author aeast
 * @see CheckSite
 * @see SiteCheckManager
 * @version 1.0
 *
 * The <code>SingleSiteChecker</code> class monitors a URL or set of URLs. It is designed to be
 * configured from and XML file using the schema in SiteChecker.xsd.
 * 
 * This class is intended to be instantiated by the <code>SiteCheckManager</code> class.
 * However, it may be run by itself in a limited capacity.
 */
public class SingleSiteChecker extends Thread {

	public static final int FAILED_CONNECT_THRESHOLD = 30;
	public static final int ALERT_ON_UP = 0;
	public static final int ALERT_ON_DOWN = 1;
	
	private String urlStr = null;
	private boolean keepChecking = true;
	private String reason = null;
	private SiteType site = null;
	private SmtpProviderType smtpProvider = null;
	private Logger logger = null;
	private long lastAlertSent = 0;
	private StringBuffer lastStatus = new StringBuffer();
	private int mode = SingleSiteChecker.ALERT_ON_DOWN;
	private boolean terminateOnAlert = false;
	
	public SingleSiteChecker() {
	}
	
	public static void main(String[] args) {
		SingleSiteChecker checker = new SingleSiteChecker();
		
		if (args.length > 0) {
			String tempStr = args[0];
			if (tempStr != null) {
				tempStr = tempStr.trim();
				if (tempStr.length() > 0 ) {
					if (tempStr.equalsIgnoreCase("-h")) {
						System.out.println( "Usage: java -jar sitecheck.jar [-h] [URL]");
					}
					else {
					    SitecheckerschemaFactoryImpl factory = new SitecheckerschemaFactoryImpl();
					    SiteType st = factory.createSiteType();
						
						st.setAlertInterval(new BigInteger("10"));
						st.setCheckInterval(new BigInteger("300"));
						st.setLogLevel("INFO");
						st.setResponseTimeThreshold(new BigInteger("60"));
						st.setUnableToConnectRetryAttempts(30);
						
						AlertListType al = factory.createAlertListType();
						
						//AlertReceiverType receiver = factory.createAlertReceiverType();
						//receiver.setEmailAddress("aeast@usatoday.com");
						
						
						List<String> l = al.getAlertReceiverEmail();
						l.add("aeast@usatoday.com");
						
						st.setAlertList(al);
						
						URLType urlType = factory.createURLType();
						urlType.setURL(tempStr);
						
						URLSequence urls = factory.createURLSequence();
						urls.getURL().add(urlType);
						
						st.setURLPath(urls);
						
						checker.initializeSite(st);
						
						
						SmtpProviderType smtpType = factory.createSmtpProviderType();
						smtpType.setFromAddress("aeast@usatoday.com");
						smtpType.setHost("usat-vocex13");
						
						checker.initializeSMTP(smtpType);
					}
				}
			}
		}
		
		checker.doIt();
	}
	
	/**
	 * 
	 */
	private void doIt() {
		
		try {

			this.logger.log(Level.INFO, "SingleSiteChecker:doIt(). Starting to check");			
			this.start();
			
			
			this.join();
			
			this.logger.log(Level.INFO, "Terminated Checking of site: " + urlStr);	
		}
		catch (Exception e) {
			this.logger.log(Level.SEVERE, "Exception in doIt()   " + e.getMessage());
			this.keepChecking = false;
		}
	}
	
	/**
	 * This method initiates the checking of a URL or set of URLs. It starts the actual checking 
	 * in a seperate thread so that it can accurately check for latency as well as other problems.
	 * 
	 */
	public void run() {
				
	    URLType urlType = site.getURLPath().getURL().iterator().next();
		logger.log(Level.INFO,"Starting Site Monitoring for site: " + urlType.getURL());

		int connectionFailureCount = 0;
		int consecutiveConnectionFailureCount = 0;
		String currentURL = null;
						
		while (keepChecking) {
			
			boolean checkNextURL = true;
					
			this.lastStatus = new StringBuffer();
			
			Iterator<URLType> itr = site.getURLPath().getURL().iterator();
			int urlIndex = 0;
			while( checkNextURL && itr.hasNext() ) {
			    URLType cURL = itr.next();
				currentURL = cURL.getURL();
								
				this.lastStatus.append("\nchecking site ").append((++urlIndex)).append(" of ").append(site.getURLPath().getURL().size()).append(":   ").append(currentURL).append("  ");
				
				this.logger.log(Level.INFO, "Checking " + currentURL + " ....");
			
				boolean siteResponding = true;
				
				CheckSite checker = new CheckSite();
				checker.setUrl(currentURL);
				if (site.getErrorPages() != null && site.getErrorPages().getErrorPage().size() > 0) {
					ArrayList<String> ePages = new ArrayList<String>();
					Iterator<String> eItr = site.getErrorPages().getErrorPage().iterator();
					while (eItr.hasNext()){
						ePages.add(eItr.next());
					}
					checker.setErrorPages(ePages);
				}
				checker.setPwd(cURL.getPwd());
				checker.setUser(cURL.getUser());
				checker.setMethod(cURL.getMethod());
				
				Thread checkerThread = new Thread(checker);		
				
				checkerThread.start();
				
				// wait for specified number of seconds
				try {
					checkerThread.join((1000*site.getResponseTimeThreshold().intValue()));
					if (checkerThread.isAlive()) {
						siteResponding = false;
					}
				}
				catch (Exception e) {
					// ignore any exception
					// get out of while loop
					keepChecking = false;
					break;
				}
				
				if (!siteResponding && (this.mode == SingleSiteChecker.ALERT_ON_DOWN)) {  // if site is not responding
					
					this.reason = "SITE NOT RESPONDING..Reason: " + checker.getResults() + " Time Taken: ";
					this.reason += (site.getResponseTimeThreshold() + " seconds");
					
					this.lastStatus.append("....Not Responding.\n");
					
					checkerThread.interrupt();
					checkerThread = null;
					
					if (this.alertRequired()) {
						this.sendAlert(currentURL);
						if (this.isTerminateOnAlert()) {
							this.keepChecking = false;
							this.reason = "Terminating because configured to terminate after an alert";
							this.cleanUp();
						}
						else {// create a new site checker in alert when up mode.
							if ( this.site.isNotifyWhenUp() ) {
								this.createSiteUpChecker(currentURL);
							}
						}
					}
					else {
						this.logger.log(Level.INFO, "Site not responding but alert suppressed due to alert frequency settings: Results==>" + checker.getResults());
					}
					checkNextURL = false;
				}
				else if ((checker.isSiteUp() == false) && (this.mode == SingleSiteChecker.ALERT_ON_DOWN)) {
					if ((checker.getHttpResponseCode() == -1) && 
					    (checker.getResults() != null) &&  
					    ((checker.getResults().indexOf("Connection reset") > 0) || (checker.getResults().indexOf("closed connection") > 0))) { // probably connection reset
						connectionFailureCount++;
						consecutiveConnectionFailureCount++;
						if (consecutiveConnectionFailureCount > site.getUnableToConnectRetryAttempts()) {
							// send alert
							this.reason = "Unable to Connect to Site after " + site.getUnableToConnectRetryAttempts() + " attempts. Reason: " + checker.getResults() + " Time Taken: " + (checker.getResponseTime()/1000) + " seconds";
							this.lastStatus.append("....Unable to Connect.\n");
						}
						else {
							// wait for 3 seconds and try again
							this.logger.log(Level.INFO, "Failed to connect. Consecutive Count: " + consecutiveConnectionFailureCount + "  TOTAL COUNT: " + connectionFailureCount);
							checkerThread = null;
							try {
								Thread.sleep(3000);
							} catch (Exception e){}
							
							// decrement url index in for loop so same page is requested again
							urlIndex--;
							continue;  // jump to next attempt without waiting
						}
					}
					else {
						// send alert
						this.reason = "SITE DOWN..Reason: " + checker.getResults() + " Time Taken: " + ((checker.getResponseTime()/1000) + " seconds");
						this.lastStatus.append(".... ERROR\n");
					}
					
					checkerThread.interrupt();
					checkerThread = null;
					
					if (this.alertRequired()) {
						this.sendAlert(currentURL);
						
						if (this.isTerminateOnAlert()) {
							this.keepChecking = false;
							this.reason = "Terminating because configured to terminate after an alert";
							this.cleanUp();
						}
						else {// create a new site checker in alert when up mode.
							if ( this.site.isNotifyWhenUp() ) {
								this.createSiteUpChecker(currentURL);
							}
						}
					}
					else {
						this.logger.log(Level.INFO, "Site down but alert suppressed due to alert frequency settings: Results==>" + checker.getResults());
					}
					checkNextURL = false;
				}
				else if(checker.isSiteUp() && (this.mode == SingleSiteChecker.ALERT_ON_UP)) { // site is up send alert if necessary
					
					this.reason = checker.getResults();
					this.lastStatus.append("....ok.");
					
					this.sendUpAlert(currentURL);
					
					if (this.isTerminateOnAlert()) {
						this.keepChecking = false;
						this.reason = "Terminating because configured to terminate after an alert";
						this.cleanUp();
					}
				}
				else {
					this.lastStatus.append("....ok.");
					this.logger.log(Level.INFO, "URL is available! It took " + (checker.getResponseTime()/1000) + " seconds to complete check. Results: " + checker.getResults());
					if (logger.isLoggable(Level.FINE)) {
						this.logger.log(Level.FINE, checker.getPageContents());
					}
					// reset consecutive failure count
					consecutiveConnectionFailureCount = 0;
				}
				
			} // end for URL Sequence
			
			if (this.keepChecking) {			
				this.logger.log(Level.INFO, "Waiting " + this.site.getCheckInterval() + " seconds before next check.");
			
				// pause before next check
				try {
					Thread.sleep((1000*site.getCheckInterval().intValue()));
				} catch (Exception ee) {}
			}
		}		
		this.logger.log( Level.INFO, "Worker Thread stopping.");
		if (this.reason != null) {
			this.logger.log(Level.INFO, "Reason:" + this.reason);
		}
	}
	
	private void createSiteUpChecker(String urlOfSite) {

		SitecheckerschemaFactoryImpl factory = new SitecheckerschemaFactoryImpl();
    
    	// Create the root element in the document using the specified root element name
    	SiteCheckerType siteChecker = factory.createSiteCheckerType();

	    SmtpProviderType iSmtpProviderType = factory.createSmtpProviderType();
	    iSmtpProviderType.setHost(this.smtpProvider.getHost());
    	iSmtpProviderType.setFromAddress(this.smtpProvider.getFromAddress());
	    siteChecker.setSmtpProvider(iSmtpProviderType);
	    
	    SiteType iSiteType = factory.createSiteType();
	    
	    URLSequence iURLSequence = factory.createURLSequence();
	    URLType iURLType = factory.createURLType();
	    iURLType.setURL(urlOfSite);
    	iURLType.setMethod("GET");
	    iURLType.setUser("");
    	iURLType.setPwd("");
	    iURLSequence.getURL().add(iURLType);   
	    iSiteType.setURLPath(iURLSequence);
	    if (this.site.getErrorPages() != null && this.site.getErrorPages().getErrorPage().size() >0){
	    	ErrorPageList iErrorList = factory.createErrorPageList();
	    	Iterator<String> itr = this.site.getErrorPages().getErrorPage().iterator();
	    	while (itr.hasNext()){
				iErrorList.getErrorPage().add(itr.next());
	    	}
			iSiteType.setErrorPages(iErrorList);
	    }
	    
	    iSiteType.setCheckInterval(new BigInteger("10"));
	    iSiteType.setResponseTimeThreshold(new BigInteger("60"));
	    iSiteType.setUnableToConnectRetryAttempts(30);
	    
	    AlertListType aList = factory.createAlertListType();
	    Iterator<String> itr = this.site.getAlertList().getAlertReceiverEmail().iterator();
	    while (itr.hasNext() ) {
	        aList.getAlertReceiverEmail().add(itr.next());
	    }
	    iSiteType.setAlertList(aList);
	    iSiteType.setAlertInterval(new BigInteger("10"));
	    iSiteType.setLogFile("");
	    iSiteType.setLogLevel("OFF");
	    iSiteType.setNotifyWhenUp(true);
	    
    	siteChecker.getSite().add(iSiteType);
				
		SingleSiteChecker tempChecker = new SingleSiteChecker();
		tempChecker.setMode(SingleSiteChecker.ALERT_ON_UP);
		tempChecker.setTerminateOnAlert(true);
		tempChecker.initializeSite(iSiteType);
		tempChecker.initializeSMTP(siteChecker.getSmtpProvider());
		
		tempChecker.start();		
	}
	
	private void sendAlert(String urlStr) {
		try {
			SmtpMailSender mail = new SmtpMailSender();
			
			mail.setMailServerHost(this.smtpProvider.getHost());
			mail.setSender(this.smtpProvider.getFromAddress());
			
			StringBuffer message = new StringBuffer();
			message.append(java.util.Calendar.getInstance().getTime().toString());
			message.append("\n");
			message.append("Automated Web Site Alert:\n\n").append(this.reason);
			message.append("\n\n");
			message.append(this.lastStatus.toString());
			
			mail.setMessageSubject(urlStr + " is down");
			mail.setMessageText(message.toString());
			
			this.logger.log(Level.INFO, "Sending Alert: " + this.reason);
			
			Iterator<String> itr = this.site.getAlertList().getAlertReceiverEmail().iterator();
			while (itr.hasNext()) {
			    
				mail.addTORecipient(itr.next().toString());
				
			}
			
			mail.sendMessage();
			
			this.setLastAlertSent(System.currentTimeMillis());
		}
		catch (Exception e) {
			this.logger.log(Level.SEVERE, "sendAlert()::Error Sending Alert" );
			this.logger.log( Level.SEVERE, e.getClass().getName() + "  " + e.getMessage());
		}
	}

	private void sendUpAlert(String urlStr) {
		try {
			SmtpMailSender mail = new SmtpMailSender();
			
			mail.setMailServerHost(this.smtpProvider.getHost());
			mail.setSender(this.smtpProvider.getFromAddress());
			
			StringBuffer message = new StringBuffer();
			message.append(java.util.Calendar.getInstance().getTime().toString());
			message.append("\n");
			message.append("Automated Web Site Alert:\n\n").append(this.reason);
			message.append("\n\n");
			message.append(this.lastStatus.toString());
			
			mail.setMessageSubject("All Clear for " + urlStr + ". Site is Up");
			mail.setMessageText(message.toString());
			
			this.logger.log(Level.INFO, "Sending Alert: " + this.reason);
			
			Iterator<String> itr = this.site.getAlertList().getAlertReceiverEmail().iterator();
			while (itr.hasNext()) {
			    String r = itr.next();
				mail.addTORecipient(r);
			}
			
			mail.sendMessage();
		}
		catch (Exception e) {
			this.logger.log(Level.SEVERE, "sendUpAlert()::Error Sending Alert" );
			this.logger.log( Level.SEVERE, e.getClass().getName() + "  " + e.getMessage());
		}
	}
	
	public boolean initializeSite(SiteType site) {
		if (site == null) {
			this.reason = "No site configuration supplied";
			return false;
		}
		
		// change this to support multiple urls
		URLType urlType = site.getURLPath().getURL().iterator().next();
		this.urlStr = urlType.getURL();
		
		try {
			new java.net.URL(this.urlStr);
		} catch (MalformedURLException e) {
			this.reason = e.getMessage();
			return false;
		}
		
		this.site = site;
		
		
		this.logger = Logger.getLogger(this.urlStr + System.currentTimeMillis());
		this.logger.setUseParentHandlers(false);
		
		String logFilePath = site.getLogFile();
		
		if ((logFilePath == null) || (logFilePath.trim().length() == 0)) {
			// use system.out
			this.logger.addHandler(new ConsoleHandler());
		}
		else {
		
			FileHandler f = null;
			try {
				f = new java.util.logging.FileHandler(site.getLogFile(), 500000, 1, true);
				f.setFormatter(new java.util.logging.SimpleFormatter());
				
				this.logger.addHandler(f);
			} catch (IOException e) {
				this.reason = "Failed To initialize log handler: " + site.getLogFile();
				return false;
			}
		}
		
		try {
			this.logger.setLevel(Level.parse(site.getLogLevel()));
		} catch (IllegalArgumentException ia) {
			// invalid logging level so use default
			this.logger.setLevel(Level.INFO);
		}
		
		this.logger.log(Level.INFO, "Site Monitor Configuration: " + new SiteDescriptionBean(site).toString());
		
		return true;
	}
	
	public boolean initializeSMTP(SmtpProviderType provider) {
		if (provider == null) {
			this.reason = "No SMTP Provider Configuration Provided";
			return false;
		}
		this.smtpProvider = provider;
		return true;
	}
	
	public void cleanUp() {
		Handler[] handlers = logger.getHandlers();
		for (int i = 0; i < handlers.length; i++) {
			Handler h = handlers[i];
			logger.removeHandler(h);
			h.close();
		}
	}
	
	private boolean alertRequired() {
		
		boolean sendAlert = false;
		
		if (this.getLastAlertSent() == 0) {
			sendAlert = true;
		}
		else {
			long currentTime = System.currentTimeMillis();
			long millisecondsSinceLastPage = currentTime - this.getLastAlertSent();
			
			// congigurable alert inteval is in minutes so convert to milliseconds
			long alertInterval = site.getAlertInterval().intValue() * 60 * 1000;
			if (millisecondsSinceLastPage >= alertInterval) {
				sendAlert = true;
			}
		}	
		
		return sendAlert;
	}
	/**
	 * Returns the keepChecking.
	 * @return boolean
	 */
	public boolean isKeepChecking() {
		return keepChecking;
	}

	/**
	 * Returns the reason.
	 * @return String
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * Returns the urlStr.
	 * @return String
	 */
	public String getUrlStr() {
		if (this.site != null) {
		    URLType url = this.site.getURLPath().getURL().iterator().next();
			return url.getURL();
		}
		return urlStr;
	}

	/**
	 * Sets the keepChecking.
	 * @param keepChecking The keepChecking to set
	 */
	public void setKeepChecking(boolean keepChecking) {
		this.keepChecking = keepChecking;
	}

	/**
	 * Sets the reason.
	 * @param reason The reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * Sets the urlStr.
	 * @param urlStr The urlStr to set
	 */
	public void setUrlStr(String urlStr) {
		this.urlStr = urlStr;
	}

	/**
	 * Returns the lastAlertSent.
	 * @return long
	 */
	public long getLastAlertSent() {
		return lastAlertSent;
	}

	/**
	 * Sets the lastAlertSent.
	 * @param lastAlertSent The lastAlertSent to set
	 */
	private void setLastAlertSent(long lastAlertSent) {
		this.lastAlertSent = lastAlertSent;
	}

	/**
	 * Returns the mode.
	 * @return int
	 */
	public int getMode() {
		return mode;
	}

	/**
	 * Returns the terminateOnAlert.
	 * @return boolean
	 */
	public boolean isTerminateOnAlert() {
		return terminateOnAlert;
	}

	/**
	 * Sets the mode.
	 * @param mode The mode to set
	 */
	public void setMode(int mode) {
		this.mode = mode;
	}

	/**
	 * Sets the terminateOnAlert.
	 * @param terminateOnAlert The terminateOnAlert to set
	 */
	public void setTerminateOnAlert(boolean terminateOnAlert) {
		this.terminateOnAlert = terminateOnAlert;
	}

}
