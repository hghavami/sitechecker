package com.usatoday.sitecheck;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import sitecheck.usatoday.com.sitecheckerschema.DocumentRoot;
import sitecheck.usatoday.com.sitecheckerschema.SiteCheckerType1;
import sitecheck.usatoday.com.sitecheckerschema.SiteType;
import sitecheck.usatoday.com.sitecheckerschema.SmtpProviderType;
import sitecheck.usatoday.com.sitecheckerschema.util.SitecheckerschemaResourceUtil;



/**
 * @author Andy East
 * @see SingleSiteChecker
 * @version 1.0
 *
 * The <code>SiteCheckerManager</code> class is a server class that is capable of monitoring 
 * any number of web sites or URLs. The input file must be a XML file that conforms to the SiteChecker.xsd 
 * schema.
 * 
 */
public class SiteCheckerManager {
	private boolean keepChecking = true;
	private java.io.File xmlConfigFile = null;
	private String xmlConfigFileName = null;
	boolean initialized = false;
	private SiteCheckerType1 scConfig = null;
	ArrayList<SingleSiteChecker> workerThreads = new ArrayList<SingleSiteChecker>();

	public SiteCheckerManager () {
		Runtime.getRuntime().addShutdownHook(new Thread() {
                public void run() {
                    endApp();
                }
            });;		
	}
	public static void main(String[] args) {
		// get the xml configuration file
		if (args.length != 1) {
			System.out.println("USAGE: java -jar sitecheck.jar [PATH TO XML CONFIG FILE]");
			System.exit(0);
		}
		
		String xmlFileName = args[0];
		if ((xmlFileName == null) || (xmlFileName.length() == 0)) {
			System.out.println("USAGE: java -jar sitecheck.jar [PATH TO XML CONFIG FILE]");
			System.exit(0);
		}
		
		File configFile = new File(xmlFileName);
		
		if(!configFile.exists() || configFile.isDirectory() || configFile.length()==0) {
			System.out.println("Invalid Configuration File Specified: " + xmlFileName);
			System.out.println("USAGE: java -jar sitecheck.jar [PATH TO XML CONFIG FILE]");
			System.exit(0);
		}
		
		// Ready to role
		SiteCheckerManager sc = new SiteCheckerManager();
		
		sc.setXmlConfigFile(configFile);
		try {
		    sc.initialize();
		
		    sc.doIt();
		}
		catch (Exception e) {
		    e.printStackTrace();
		    System.out.println(e.getMessage());
		}
	} 
	
	private void initialize() throws Exception {
		if (this.getXmlConfigFile() == null) {
			return;
		}
		
		// Set system properties
		//System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol");
		//System.setProperty("java.protocol.handler.pkgs", "javax.net.ssl");
		// Add provider
		//java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());

		//SSLUtilities.trustAllHostnames();
		//SSLUtilities.trustAllHttpsCertificates();
		
		SitecheckerschemaResourceUtil scFactory = SitecheckerschemaResourceUtil.getInstance();
   		 // Load the document
		File configFile = new File(this.getXmlConfigFileName());
		
		FileInputStream is = new FileInputStream(configFile);
		
		DocumentRoot root = scFactory.load(is);
		
   		this.scConfig = root.getSiteChecker();
   		
		SmtpProviderType smtpProvider = this.scConfig.getSmtpProvider();

		//Create Thread Pool
		Iterator<SiteType> itr = this.scConfig.getSite().iterator();
		while (itr.hasNext() ) {
			SiteType site = itr.next();
			// create the individual site check object
			SingleSiteChecker siteChecker = new SingleSiteChecker();
			
			if( siteChecker.initializeSMTP(smtpProvider) && siteChecker.initializeSite(site)) {
				this.workerThreads.add(siteChecker);
			}
			else {
				System.out.println("Unable to initialize workder thread for site: " + site.toString());
				System.out.println("REASON: " + siteChecker.getReason());
			}
		}
		
		this.scConfig = null;
		this.initialized = true;
	}
	
	private void doIt() {
		if (!this.initialized) {
			System.out.println( "Site Checker not initialized..Exiting.");
			return;
		}
		
		boolean terminateProcess = false;
		
		System.out.println( "Enter \"R\" to refresh configuration OR any key to terminate checking");
		System.out.println();

		while (!terminateProcess) {				
			// start threads
			for (int i = 0; i < this.workerThreads.size(); i++ ) {
				Thread t = this.workerThreads.get(i);
				t.start();
			}
			this.keepChecking = true;
			
			int b = 0;
			while (this.keepChecking) {
				try {
					b = System.in.read();
					System.in.skip(500);
				} catch (IOException e) {
					System.out.println("Error Reading Standard Input.");
					e.printStackTrace();
				}
				
				
				if (b==82 || b==114) { // user entered an R
					System.out.println("Reset Initialization Requested and Processing..");
					this.cleanupThreads();
					try {
					    this.initialize();
					}
					catch (Exception e) {
					    e.printStackTrace();
					    System.out.println(e.getMessage());
					}
					this.keepChecking = false;
				}
				else {
					System.out.println();
					System.out.println( "Shutdown initiated....");
					
					terminateProcess = true;
					
					// set flag for thread to stop
					this.keepChecking = false;
					this.cleanupThreads();
				} // end else			
			}	// end while keep checking
		} // end while terminate process
	}
	
	private void endApp() {
		System.out.println("Application terminating");
		this.cleanupThreads();
	}
	
	private void cleanupThreads() {
		if (this.workerThreads.size() == 0) {
			return;
		}
		
		// stop threads
		for (int j=0; j < this.workerThreads.size(); j++) {
			SingleSiteChecker worker = this.workerThreads.get(j);
			worker.setKeepChecking(false);
			worker.setReason("User Requested Shutdown");
			worker.interrupt();	
			try {		
				worker.join();
			} catch (InterruptedException ie) {
				System.out.println("Manager thread interrupted on thread index: " + j);
			}
			worker.cleanUp();
		}
		this.workerThreads.clear();
	}
	
	/**
	 * Returns the xmlConfigFile.
	 * @return java.io.File
	 */
	public java.io.File getXmlConfigFile() {
		return xmlConfigFile;
	}

	/**
	 * Returns the xmlConfigFileName.
	 * @return String
	 */
	public String getXmlConfigFileName() {
		return xmlConfigFileName;
	}

	/**
	 * Sets the xmlConfigFile.
	 * @param xmlConfigFile The xmlConfigFile to set
	 */
	public void setXmlConfigFile(java.io.File xmlConfigFile) {
		this.xmlConfigFile = xmlConfigFile;
		this.setXmlConfigFileName(xmlConfigFile.getAbsolutePath());
	}

	/**
	 * Sets the xmlConfigFileName.
	 * @param xmlConfigFileName The xmlConfigFileName to set
	 */
	private void setXmlConfigFileName(String xmlConfigFileName) {
		this.xmlConfigFileName = xmlConfigFileName;
	}
		
}
