package com.usatoday.sitecheck;
 
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collection;
import java.util.Iterator;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;

import sun.net.ftp.FtpClient;
import sun.net.www.protocol.ftp.FtpURLConnection;

/**
 * @author Andy East
 * @version 1.0
 *
 * Class <code>CheckSite</code> is designed to open a connection to a URL and 
 * determine if the page is available. The class supports the HTTP, HTTPS,and FTP
 * protocols.  The class is designed to be run in a seperate thread but this is not
 * necessary.
 * <pre>
 * Sample usage:
 * 		CheckSite cs = new CheckSite();
 * 		cs.setUrl("http://www.google.com"); // Protocol is required
 * 		cs.setUser("user");   				// optional, if a user id is required
 * 		cs.setPwd( "password");				// optional, if a password is required
 * 		cs.setMethod( "POST" );				// optional, default is GET not used for all protocols
 * 
 * 		cs.run();  // check the site
 * 
 * 		if (cs.isSiteUp()) {
 * 			// do some work
 * 		}
 * 		else {
 * 			// do some different work  - http responses do not always apply
 * 			System.out.println(cs.getResutls()+cs.getHttpResponseMessage()+cs.getHttpResponseCode());
 * 		}
 * </pre>
 */
public class CheckSite implements Runnable {

	private String urlStr = null;
	private Collection<String> errorPages = null;
	private String method = "GET";
	private String user = null;
	private String pwd = null;
	private String results = "No Check Performed";
	private boolean siteUp = false;
	private long responseTime = -1;
	private String httpResonseMessage = null;
	private int httpResponseCode = -1;
	private StringBuffer pageContents = new StringBuffer();
	
	/**
	 * Constructor for CheckSite.
	 * This class is used for connecting to a site and then 
	 * disconnecting.
	 */
	public CheckSite() {
		super();
	}

	/**
	 * @see java.lang.Runnable#run()
	 * 
	 * This method initiates the check
	 */
	public void run() {
		
		if (this.getUrl() == null) {
			this.setSiteUp(false);
			this.setResults("No URL Specified to check");
		}
		else {
			this.setResults("Waiting For Response.");
			this.siteRespondingV2();
			//this.siteResponding();
		}
	}

	private void siteRespondingV2() {
//	  Create an instance of HttpClient.
	    
	    HttpClient client = new HttpClient();
	    
		long startTime =0;
		long endTime = 0;

		String additionalInfo = "";
		
	    GetMethod method = null;
	    Exception ee = null;
	    int statusCode = -99;
	    try {
			startTime = System.currentTimeMillis();

			// Create a method instance.
		    method = new GetMethod(this.generateCompleteURLStr());
		    //method.setFollowRedirects(true);
		    // Provide custom retry handler is necessary
		    //method.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, 
		    	//	new DefaultHttpMethodRetryHandler(3, false));

	      // Execute the method.
	      statusCode = client.executeMethod(method);

	      if (statusCode != HttpStatus.SC_OK) {
	        System.err.println("Method failed: " + method.getStatusLine());
	        this.setHttpResonseMessage(method.getStatusText());
	        this.setHttpResponseCode(statusCode);
	        this.setSiteUp(false);
	      }
	      else {
		      // Read the response body.
		      byte[] responseBody = method.getResponseBody();

		      // Deal with the response.
		      // Use caution: ensure correct character encoding and is not binary data
		      additionalInfo = new String(responseBody);
		      additionalInfo = additionalInfo.replaceAll("\r","");
		      additionalInfo = additionalInfo.replaceAll("\n", " ");

		      this.setSiteUp(true);
		      this.setHttpResonseMessage(method.getStatusLine().toString());
		      this.setHttpResponseCode(statusCode);
	      }
	      this.setResults("Response : " + method.getStatusLine().toString() + "   : " + additionalInfo);

	    } catch (HttpException e) {
	        ee = e;
	      System.err.println("Fatal protocol violation: " + e.getMessage());
	      e.printStackTrace();
	    } catch (IOException e) {
	        ee = e;
	      System.err.println("Fatal transport error: " + e.getMessage());
	      e.printStackTrace();
	    } 
	    catch (Exception e) {
	        ee = e;
		      System.err.println("Fatal transport error: " + e.getMessage());
		      e.printStackTrace();
	    }
	    finally {
	      // Release the connection.
	      method.releaseConnection();
	    }  
	    
	    if (ee!= null) {
			int code = -1;
			String msg = "";
			//StringBuffer msgBuffer = new StringBuffer();
				
			if (client != null) {
				code = statusCode;
			}
			
			this.setResults(java.util.Calendar.getInstance().getTime().toString() + "    Exception(s): " + ee.getMessage() + " -- URL RESPONSE CODE: " + code + " URL RESPONSE MESSAGE: " + msg);
			// sometimes the connection is reset after successful connection. Don't send alert
			if (client != null && statusCode == HttpURLConnection.HTTP_OK) {
				this.setSiteUp(true);
			}			
			else {	
				this.setSiteUp(false);
			}
	    }
		endTime = System.currentTimeMillis();
		this.setResponseTime(endTime-startTime);	    
	}
	
	/**
	 * This method performs the work of checking the URL
	 */
	@SuppressWarnings("unused")
	private void siteResponding() {		
		java.net.URL url = null;		
		
		java.net.HttpURLConnection urlConnection = null;
		URLConnection urlc = null;
		FtpURLConnection ftpc = null;
		
		long startTime =0;
		long endTime = 0;
				
		try {
			startTime = System.currentTimeMillis();
			
			url = new java.net.URL(this.generateCompleteURLStr());
			
			urlc = url.openConnection();
			if (urlc instanceof HttpURLConnection) {
				urlConnection = (java.net.HttpURLConnection)urlc;
				if (urlConnection instanceof javax.net.ssl.HttpsURLConnection) {
					javax.net.ssl.HttpsURLConnection sc = (javax.net.ssl.HttpsURLConnection)urlConnection;
					//sc.setHostnameVerifier(new IgnoreCertificateVerifier());
				}
				this.processHttpProtocol(urlConnection);
			}
			else if (urlc instanceof FtpURLConnection) {
				ftpc = (FtpURLConnection)urlc;
				this.processFtpProtocol(ftpc);
			}
		
		}
		catch (Exception e) {
			int code = -1;
			String msg = "";
			//StringBuffer msgBuffer = new StringBuffer();
				
			if (urlConnection != null) {
				try {
					code = urlConnection.getResponseCode();
					msg = urlConnection.getResponseMessage();
					this.setHttpResonseMessage(urlConnection.getResponseMessage());
					this.setHttpResponseCode(urlConnection.getResponseCode());
				} catch (Exception ee){}
			}
			
			try {
				this.setResults(java.util.Calendar.getInstance().getTime().toString() + "    Exception(s): " + e.getMessage() + " -- URL RESPONSE CODE: " + code + " URL RESPONSE MESSAGE: " + msg);
				// sometimes the connection is reset after successful connection. Don't send alert
				if (urlConnection != null && urlConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {
					this.setSiteUp(true);
				}			
				else {	
					this.setSiteUp(false);
				}
			}
			catch (IOException ee) {
				this.setResults(java.util.Calendar.getInstance().getTime().toString() + "    Exception(s): " + e.getMessage() + " -- URL RESPONSE CODE: " + code + " URL RESPONSE MESSAGE: " + msg);
				this.setSiteUp(false);
			}
		}		
		
		endTime = System.currentTimeMillis();
		this.setResponseTime(endTime-startTime);
	}
	
	/**
	 * This method handles the HTTP and HTTPS protocols
	 */
	private void processHttpProtocol(HttpURLConnection urlConnection) throws Exception {
		// set configuration parameters on connection	
		HttpURLConnection.setFollowRedirects(true);
		urlConnection.setRequestMethod(this.getMethod());
		
		// open the connection
		urlConnection.connect();									

		int totalBytes = 0;

		int maxRedirections = 10;
		int redirectCount = 0;
		
		// Handle the 302 redirects
		while((redirectCount < maxRedirections) && (urlConnection.getResponseCode()==HttpURLConnection.HTTP_MOVED_TEMP)) {
			redirectCount++;
			String locationStr = urlConnection.getHeaderField("Location");
			String redirectURLStr = null;
			// if relative URL
			if (locationStr.charAt(0) == '/') {
				redirectURLStr = this.urlStr + locationStr;
			}
			else {
				redirectURLStr = locationStr;
			}
			URL url = new java.net.URL(redirectURLStr);
			String rFile = url.getFile();
			
			// Determine if page is one of the error pages
			if (isErrorPage(rFile)) {
				this.setHttpResonseMessage(urlConnection.getResponseMessage());
				this.setHttpResponseCode(urlConnection.getResponseCode());
				this.setSiteUp(false);
				this.setResults("Landed on Error Page (possible redirect). WAS Service may not be available. Error Page: " + rFile);
				urlConnection.disconnect();
				urlConnection = null;
				return;			
			}	
					 
			urlConnection = (HttpURLConnection)url.openConnection();
		}
		
		int contentLength = urlConnection.getContentLength();
		if (contentLength == -1) {
			contentLength = 5120;
		}
		
		if (urlConnection.getResponseCode() < 400) {
			try {
				java.io.InputStream is = urlConnection.getInputStream();
				
				byte [] b = new byte[contentLength+10];
				
				int bytesRead = 0;
				
				while( (bytesRead = is.read(b, 0, contentLength)) > 0) {
					totalBytes += bytesRead;
					if (bytesRead > 0) {
						this.pageContents.append(new String(b, 0, (bytesRead-1)));
					}			
				}	
				
				is.close();
			}
			catch (Exception e) {
				throw e;
			}
		}
		
		this.setHttpResonseMessage(urlConnection.getResponseMessage());
		this.setHttpResponseCode(urlConnection.getResponseCode());
		
		if (urlConnection.getResponseCode() >= HttpURLConnection.HTTP_BAD_REQUEST) {
			this.setSiteUp(false);
			//System.out.println("No Data Read");
			this.setResults(totalBytes + " bytes read from site. Response Code: " + urlConnection.getResponseCode());
			this.setSiteUp(false);
		}
		else {
			this.setResults("Response Code: " + urlConnection.getResponseCode() + " Response Message: " + urlConnection.getResponseMessage());
			this.setSiteUp(true);
		}
			
		urlConnection.disconnect();
		urlConnection = null;
	}
	
	/**
	 * This method handles the FTP protocol
	 */
	private void processFtpProtocol(FtpURLConnection ftpConn) throws Exception {
		
		FtpClient ftpClient = new FtpClient();
		
		ftpClient.openServer(ftpConn.getURL().getHost());
		
		if (ftpClient.serverIsOpen()) {
			
			// at this point the site is available even if user supplied incorrect password
			this.setSiteUp(true);

			if (this.getUser() != null) {			
				ftpClient.login(this.getUser(), this.getPwd());
				
				String rStr = ftpClient.getResponseString();
				
				this.results = "Connected to FTP Client: Response String: " + rStr;
			}
			else {
				this.results = "Connected to FTP Client but did not log into server";
			}
			
			ftpClient.closeServer();  
		}
		else {
			this.results = "Unable to connect to FTP Server: " + ftpConn.getURL().getHost();
		}
	}
	
	/**
	 * this method generates the full URL string from the url/userid/password
	 * @return String
	 */
	private String generateCompleteURLStr() throws Exception {

		// if no user name provided then assume not necessary		
		if (this.user == null) {
			return this.urlStr;
		}
		
		URL tempurl = new URL(this.urlStr);

		StringBuffer strbuf = new StringBuffer();
		String t = null;
		
		strbuf.append(tempurl.getProtocol()).append("://");
		strbuf.append(this.user);
		if (this.pwd != null && this.pwd.length()>0) {
			strbuf.append(":").append(this.pwd);
		}
		strbuf.append("@");
		strbuf.append(tempurl.getHost()).append(":").append(tempurl.getPort());
		t = tempurl.getFile();
		if (t != null) {
			strbuf.append(tempurl.getFile());
		}
		return strbuf.toString();
	}
	
	private boolean isErrorPage(String urlFileStr) {
		boolean isErrorPage = false;
		
		if (urlFileStr != null && this.errorPages != null && this.errorPages.size()>0){
			Iterator<String> itr = this.errorPages.iterator();
			while(itr.hasNext()){
				String ePage = itr.next();
				if (urlFileStr.equalsIgnoreCase(ePage)) {
					isErrorPage = true;
					break;
				}
			}
		}
		
		return isErrorPage;
	}	
	/**
	 * Returns the results.
	 * @return String
	 */
	public String getResults() {
		return results;
	}

	/**
	 * Returns the url.
	 * @return String
	 */
	public String getUrl() {
		return urlStr;
	}

	/**
	 * Sets the results.
	 * @param results The results to set
	 */
	private void setResults(String results) {
		this.results = results;
	}

	/**
	 * Sets the url.
	 * @param url The url to set
	 */
	public void setUrl(String url) {
		this.urlStr = url;
	}

	/**
	 * Returns the siteUp.
	 * @return boolean
	 */
	public boolean isSiteUp() {
		return siteUp;
	}

	/**
	 * Sets the siteUp.
	 * @param siteUp The siteUp to set
	 */
	private void setSiteUp(boolean siteUp) {
		this.siteUp = siteUp;
	}

	/**
	 * Returns the responseTime.
	 * @return long
	 */
	public long getResponseTime() {
		return responseTime;
	}

	/**
	 * Sets the responseTime.
	 * @param responseTime The responseTime to set
	 */
	private void setResponseTime(long responseTime) {
		this.responseTime = responseTime;
	}

	/**
	 * Returns the httpResonseMessage.
	 * @return String
	 */
	public String getHttpResonseMessage() {
		return httpResonseMessage;
	}

	/**
	 * Returns the httpResponseCode.
	 * @return int
	 */
	public int getHttpResponseCode() {
		return httpResponseCode;
	}

	/**
	 * Sets the httpResonseMessage.
	 * @param httpResonseMessage The httpResonseMessage to set
	 */
	private void setHttpResonseMessage(String httpResonseMessage) {
		this.httpResonseMessage = httpResonseMessage;
	}

	/**
	 * Sets the httpResponseCode.
	 * @param httpResponseCode The httpResponseCode to set
	 */
	private void setHttpResponseCode(int httpResponseCode) {
		this.httpResponseCode = httpResponseCode;
	}

	/**
	 * Returns the pageContents.
	 * @return StringBuffer
	 */
	public String getPageContents() {
		return pageContents.toString();
	}

	/**
	 * Returns the pwd.
	 * @return String
	 */
	public String getPwd() {
		return pwd;
	}

	/**
	 * Returns the user.
	 * @return String
	 */
	public String getUser() {
		return user;
	}

	/**
	 * Sets the pwd.
	 * @param pwd The pwd to set
	 */
	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	/**
	 * Sets the user.
	 * @param user The user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * Returns the method.
	 * @return String
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * Sets the method.
	 * @param method The method to set
	 */
	public void setMethod(String method) {
		if (method != null && method.length() > 0) {
			this.method = method;
		}
	}

	/**
	 * @return
	 */
	public Collection<String> getErrorPages() {
		return errorPages;
	}

	/**
	 * @param collection
	 */
	public void setErrorPages(Collection<String> collection) {
		errorPages = collection;
	}

}
