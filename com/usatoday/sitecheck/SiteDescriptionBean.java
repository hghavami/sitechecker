/*
 * Created on Feb 27, 2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package com.usatoday.sitecheck;

import java.util.Iterator;

import sitecheck.usatoday.com.sitecheckerschema.AlertListType;
import sitecheck.usatoday.com.sitecheckerschema.SiteType;
import sitecheck.usatoday.com.sitecheckerschema.URLType;


/**
 * @author aeast
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class SiteDescriptionBean {

	SiteType site = null;
	/**
	 * 
	 */
	public SiteDescriptionBean(SiteType site) {
		super();
		this.site = site;
	}
	
	/**
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringBuffer config = new StringBuffer();
		
		if (this.site == null){
			config.append("No valid site specified");
			return config.toString();
		}
		
		config.append("Monitoring the following URLS: \n");
		Iterator<URLType> itr = this.site.getURLPath().getURL().iterator();
		while( itr.hasNext() ) {
			URLType url = (URLType)itr.next();
			config.append("\t").append(url.getURL()).append("...Request Method: ").append(url.getMethod());
			if (url.getUser() != null) {
				config.append("...User: ").append(url.getUser());
			}
			if (url.getPwd() != null) {
				config.append("...Password: ").append("*******");
			} 
			config.append("\n");
		}
		
		config.append("Check Interval (Seconds): ").append(this.site.getCheckInterval());
		config.append("\nLatency Timeout (Seconds): ").append(this.site.getResponseTimeThreshold());
		config.append("\nFailed connection attempts: ").append(this.site.getUnableToConnectRetryAttempts());
		config.append("\nAlert Frequecy (minutes): ").append(this.site.getAlertInterval());
		if (this.site.getLogFile() != null && this.site.getLogFile().trim().length() > 0) {
			config.append("\nLog File: ").append(this.site.getLogFile());
		}
		else {
			config.append("\nLog File: Console Output Only");
		}
		config.append("\nLog Level: ").append(this.site.getLogLevel());
		
		if (this.site.getErrorPages() != null && this.site.getErrorPages().getErrorPage().size() > 0){
			config.append("\nError Pages: ");
			Iterator<String> itr1 = this.site.getErrorPages().getErrorPage().iterator();
			while (itr1.hasNext()){
			    
				config.append(itr1.next());
				if (itr1.hasNext()) {
					config.append(", ");
				}
			}
		}
		else {
			config.append("\nNo Error Pages Specified");
		}
		
		config.append("\nAlert Receiver(s):\n");
		AlertListType al = this.site.getAlertList();
		Iterator<String> itr2 = al.getAlertReceiverEmail().iterator();
		while (itr2.hasNext()) {
		    Object o = itr2.next();
			config.append("\t").append(o.toString()).append("\n");
		}
		return config.toString();
	}  
}
